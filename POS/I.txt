I, noun
i, symb.
id, noun
if, conjunction, noun
in, preposition, adverb, adjective, noun
Io
is
it, pronoun
IBA, abbreviation
Ibo, noun, adjective
ice, noun, verb
icy, adjective
Ida
Ido, noun
Ife
Ijo, noun, adjective
ILA, abbreviation
ilk, noun
ill, adjective, adverb, noun
imp, noun, verb
IND, abbreviation
ink, noun, verb
inn, noun
ion, noun
IRA, abbreviation
ire, noun
irk, verb
ism, noun
ITA, abbreviation
its, poss.
ivy, noun
iamb, noun
Iban, noun, adjective
ibex, noun
ibis, noun
iced, adjective
icon, noun
idea, noun
ides, pl.
idle, adjective, verb
idly, adverb
idol, noun
i.e., abbreviation
iffy, adjective
iiwi, noun
ikat, noun
ilex, noun
ilia
imam, noun
impi, noun
Inca, noun
inch, noun, verb
Indy, noun
inky, adjective
inly, adverb
inro, noun
into, preposition
iota, noun
Iowa
Iran
Iraq
iris, noun, verb
iron, noun, verb
Isis
isle, noun
iso-, comb.
itch, noun, verb
item, noun, adverb
Ito,
Ivan
ixia, noun
Iceni, plural
ichor, noun
icing, noun
ictus, noun
Idaho
ideal, adjective, noun
idiom, noun
idiot, noun
idler, noun
idyll, noun
igloo, noun
ileum, noun
ileus, noun
iliac, adjective
Iliad
ilium, noun
image, noun, verb
imago, noun
embed, verb, noun
imbue, verb
imide, noun
imine, noun
impel, verb
imply, verb
imshi, exclamation
inane, adjective
inapt, adjective
incog, adjective, adverb, 
incur, verb
incus, noun
index, noun, verb
India
Indic, adjective, noun
indie, adjective, noun
Indra
indri, noun
endue, verb
Indus
inept, adjective
inert, adjective
infer, verb
infix, verb, noun
infra, adverb
ingle, noun
ingot, noun
inion, noun
Injun, noun
inkle, noun
inlay, verb, noun
inlet, noun
inner, adjective, noun
input, noun, verb
inset, noun, verb
inter, verb
inure, verb
inurn, verb
Invar, noun
iodic, noun
iodo-, comb.
Ionic, adjective, noun
ionic, adjective
irade, noun
Iraqi, noun, adjective
irate, adjective
Irgun
Irish, adjective, noun
iroko, noun
irony, noun
Isaac
Islam, noun
Islay
islet, noun
issei, noun
issue, noun, verb
ixtle, noun
itchy, adjective
ivied, adjective
ivory, noun
Ixion
izard, noun
iambus, noun
iambic, adjective, noun
Iberia
iberis, noun
Icarus
icebox, noun
iceman, noun
icicle, noun
iconic, adjective
ideate, verb
idiocy, noun
ignite, verb
ignore, verb
iguana, noun
illude, verb
illume, verb
imager, noun
imbibe, verb
imbrue, verb
immune, adjective
immure, verb
impact, noun, verb
impair, verb
impala, noun
impale, verb
impark, verb
impart, verb
impede, verb
impend, verb
impish, adjective
import, verb, noun
impose, verb
impost, noun
impugn, verb
impure, adjective
impute, verb
inanga, noun
inarch, verb
inborn, adjective
inbred, adjective
encase, verb
incept, verb
incest, noun
incise, verb
incite, verb
income, noun
incuse, noun, verb, adjective
indaba, noun
indeed, adverb
indene, noun
indent, verb, noun
Indian, adjective, noun
indict, verb
indigo, noun
indite, verb
indium, noun
indole, noun
indoor, adjective
induce, verb
induct, verb
indult, noun
induna, noun
infall, noun
infamy, noun
infant, noun
infect, verb
infeed, noun
infest, verb
infill, noun, verb
infirm, adjective
inflow, noun
influx, noun
infold, verb
enfold, verb
inform, verb
infula, noun
infuse, verb
ingest, verb
ingulf, verb
Ingush, noun, adjective
inhale, verb
inhaul, noun
inhere, verb
inhume, verb
inject, verb
injure, verb
injury, noun
inlaid
inland, adjective, adverb, noun
inlier, noun
inmate, noun
inmost, adjective
innate, adjective
inning, noun
inroad, noun
inrush, noun
insane, adjective
inseam, noun
insect, noun
insert, verb, noun
inside, noun, adjective, preposition
insist, verb
insole, noun
inspan, verb
instar, noun
instep, noun
insula, noun
insult, verb, noun
insure, verb
intact, adjective
intake, noun
intend, verb
intent, noun, adjective
intern, noun, verb
intima, noun
intone, verb
intuit, verb
inulin, noun
invade, verb
invent, verb
invert, verb, noun
invest, verb
invite, verb, noun
invoke, verb
inwale, noun
inward, adjective, adverb
enwrap, verb
iodide, noun
iodine, noun
iodism, noun
iodize, verb
Ionian, noun, adjective
ionize, verb
ipecac, noun
irenic, adjective, noun
iritis, noun
ironic, adjective
irrupt, verb
Irving
Isaiah
isatin, noun
island, noun, verb
isobar, noun
isohel, noun
Isolde
isomer, noun
isopod, noun
Israel
Italic, adjective, noun
italic, adjective, noun
Ithaca
itself, pronoun
Iapetus
Iberian, adjective, noun
iceberg, noun
iceboat, noun
icefall, noun
icefish, noun
Iceland
ichthus, noun
icterus, noun
ideally, adverb
idiotic, adjective
idolize, verb
idyllic, adjective
igneous, adjective
igniter, noun
ignoble, adjective
iguanid, noun
ileitis, noun
iliacus, noun
illegal, adjective, noun
illicit, adjective
illness, noun
illogic, noun
Ilocano, noun, adjective
ilvaite, noun
imagery, noun
imagine, verb
imagism, noun
imitate, verb
immense, adjective
immerse, verb
immoral, adjective
impanel, verb
impasse, noun
impasto, noun
impeach, verb
imperil, verb
impetus, noun
impiety, noun
impinge, verb
impious, adjective
implant, verb, noun
implead, verb
implied, adjective
implore, verb
implode, verb
impound, verb
impress, verb, noun
imprest, noun
imprint, verb, noun
improve, verb
impulse, noun
inboard, adverb, noun
inbound, adjective, verb
inbreed, verb
inbuilt, adjective
incense, noun, verb
incisor, noun
incline, verb, noun
enclose, verb
include, verb
incomer, noun
inconnu, noun
encrust, verb
incubus, noun
incudes
incurve, verb
indeedy, adverb
Indiana
indican, noun
indices
indicia, pl.
indoors, adverb, noun
endorse, verb
indoxyl, noun
indraft, noun
indrawn, adjective
indulge, verb
indwell, verb
inertia, noun
inexact, adjective
infancy, noun
infanta, noun
infante, noun
infarct, noun
inferno, noun
infidel, noun, adjective
infield, noun, adverb
inflame, verb
inflate, verb
inflect, verb
inflict, verb
ingénue, noun
ingesta, pl.
ingoing, adjective
engraft, verb
ingrain, verb, adjective
ingrate, noun, adjective
ingress, noun
ingrown, adjective
inhabit, verb
inhaler, noun
inherit, verb
inhibit, verb
inhuman, adjective
initial, adjective, noun, verb
injunct, verb
injured, adjective
inkhorn, noun
inkling, noun
inkwell, noun
inlying, adjective
Inness,
inquest, noun
inquire, verb
inquiry, noun
inshore, adjective, adverb
insider, noun
insight, noun
insipid, adjective
insofar, adverb
inspect, verb
inspire, verb
install, verb
instant, adjective, noun
instate, verb
instead, adverb
instill, verb
insular, adjective
insulin, noun
insured, adjective, noun
insurer, noun
integer, noun
intense, adjective
interim, noun, adjective, adverb
introit, noun
intrude, verb
intrust, verb
inutile, adjective
invader, noun
invalid, noun, verb
inveigh, verb
inverse, adjective, noun
invoice, noun, verb
involve, verb
inwards, adverb
ionizer, noun
ipomoea, noun
Iranian, adjective, noun
iridium, noun
irksome, adjective
ironist, noun
ironize, verb
ischium, noun
Isfahan
Ishmael
Islamic, adjective
Ismaili, noun
isogamy, noun
isohyet, noun
isolate, verb, noun
Isopoda
isotope, noun
Israeli, adjective, noun
issuant, adjective
isthmus, noun
Italian, noun, adjective
itemize, verb
iterate, verb, noun
ivorine, noun
ibisbill, noun
iceblink, noun
icebound, adjective
icehouse, noun
ichthyic, adjective
idealism, noun
idealist, noun
ideality, noun
idealize, verb
ideation, noun
identify, verb
identity, noun
ideogram, noun
ideology, noun
idiotype, noun
idocrase, noun
idolater, noun
idolatry, noun
ignition, noun
ignitron, noun
ignominy, noun
ignorant, adjective
illation, noun
illative, adjective, noun
Illinois
illiquid, adjective
illumine, verb
illusion, noun
illusive, adjective
illusory, adjective
Illyrian, noun, adjective
ilmenite, noun
imaginal, adjective
imagines
imbecile, noun, adjective
imitator, noun
immanent, adjective
Emmanuel
immature, adjective
imminent, adjective
immobile, adjective
immodest, adjective
immolate, verb
immortal, adjective, noun
immotile, adjective
immunity, noun
immunize, verb
impacted, adjective
impeller, noun
imperial, adjective, noun
imperium, noun
impetigo, noun
implicit, adjective
impolite, adjective
imposing, adjective
impostor, noun
impotent, adjective
imprison, verb
improper, adjective
impudent, adjective
impunity, noun
impurity, noun
inaction, noun
inactive, adjective
inasmuch, adverb
incenter, noun
inchmeal, adverb
inchoate, adjective
inchworm, noun
incident, noun, adjective
incision, noun
incisive, adjective
incisure, noun
included, adjective
incoming, adjective, noun
increase, verb, noun
increate, adjective
incubate, verb
incubous, adjective
indebted, adjective
indecent, adjective
indented, adjective
indenter, noun
Indiaman, noun
indicant, noun
indicate, verb
indigene, noun
indigent, adjective, noun
indigoid, adjective
indirect, adjective
indocile, adjective
indolent, adjective
Indology, noun
inductor, noun
induline, noun
indurate, verb
indusium, noun
industry, noun
inedible, adjective
inedited, adjective
inequity, noun
inerrant, adjective
inertial, adjective
inexpert, adjective
infamous, adjective
infantry, noun
infecund, adjective
inferior, adjective, noun
infernal, adjective
infinite, adjective, noun
infinity, noun
inflated, adjective
inflexed, adjective
influent, adjective, noun
informal, adjective
informed, adjective
informer, noun
infrared, adjective, noun
infringe, verb
infusion, noun
ingather, verb
ingrowth, noun
inguinal, adjective
inhalant, noun, adjective
inherent, adjective
inhesion, noun
inhumane, adjective
inimical, adjective
iniquity, noun
initiate, verb, noun
injector, noun
inkberry, noun
inkstand, noun
innocent, adjective, noun
innovate, verb
innuendo, noun
inoculum, noun
inositol, noun
in-phase, adjective
insanity, noun
inscribe, verb
insectan, adjective
insecure, adjective
insignia, noun
insolent, adjective
insomnia, noun
insomuch, adverb
inspired, adjective
inspirit, verb
instance, noun, verb
instancy, noun
instinct, noun, adjective
instruct, verb
insulant, noun
insulate, verb
intaglio, noun, verb
intarsia, noun
integral, adjective, noun
intended, adjective, noun
intently, adverb
interact, verb
interbed, verb
intercom, noun
interest, noun, verb
interior, adjective, noun
interlap, verb
interlay, verb, noun
interlay, verb, noun
intermit, verb
intermix, verb
internal, adjective, pl.
internee, noun
intersex, noun
interval, noun
interwar, adjective
inthrall, verb
intimacy, noun
intimate, adjective, noun
intitule, verb
intonate, verb
intrados, noun
entrench, verb
intrepid, adjective
intrigue, verb, noun
introrse, adjective
intruder, noun
intubate, verb
inundate, verb
invasion, noun
invasive, adjective
invected, adjective
inveigle, verb
inventor, noun
inverter, noun
inviable, adjective
inviscid, adjective
inviting, adjective
involute, adjective, noun, verb
involved, adjective
inwardly, adverb
iodinate, verb
iodoform, noun
irenicon, noun
Irishman, noun
ironbark, noun
ironclad, adjective, noun
ironware, noun
ironwood, noun
ironwork, noun
Iroquois, noun, adjective
irrigate, verb
irritant, noun, adjective
irritate, verb
Isabella, noun
ischemia, noun
Islamism, noun
islander, noun
islesman, noun
isobutyl, noun
isocheim, noun
isochron, noun
isocline, noun
isogenic, adjective
isogloss, noun
isogonic, adjective
isolated, adjective
isomorph, noun
isopleth, noun
isoprene, noun
Isoptera
isostasy, noun
isothere, noun
isotherm, noun
isotonic, adjective
isthmian, adjective
Icelander, noun
Icelandic, adjective, noun
ichneumon, noun
ichthyoid, adjective, noun
iconodule, noun
iconology, noun
identical, adjective
ideograph, noun
ideologue, noun
idiomatic, adjective
idiopathy, noun
Idomeneus
ignoramus, noun
ignorance, noun
iguanodon, noun
ileostomy, noun
illegible, adjective
illiberal, adjective
Illinoian, adjective
illogical, adjective
imaginary, adjective
imbalance, noun
imbricate, verb, adjective
imbroglio, noun
imidazole, noun
imitation, noun
imitative, adjective
immediacy, noun
immediate, adjective
immensely, adverb
immersion, noun
immersive, adjective
immigrant, noun
immigrate, verb
immixture, noun
immovable, adjective, noun
immutable, adjective
impaction, noun
impartial, adjective
impassion, verb
impassive, adjective
impatiens, noun
impatient, adjective
impedance, noun
imperator, noun
imperfect, adjective, noun
imperious, adjective
impetrate, verb
impetuous, adjective
implement, noun, verb
implicate, verb, noun
implosive, adjective
impluvium, noun
impolitic, adjective
important, adjective
importune, verb
imposture, noun
imprecate, verb
imprecise, adjective
improbity, noun
impromptu, adjective, noun
improvise, verb
imprudent, adjective
impudence, noun
impulsion, noun
impulsive, adjective
inability, noun
inamorata, noun
inamorato, noun
inanimate, adjective
inanition, noun
inaudible, adjective
inaugural, adjective, noun
inbreathe, verb
incapable, adjective
incarnate, adjective, verb
incensory, noun
incentive, noun
inception, noun
inceptive, adjective, noun
incessant, adjective
incidence, noun
incipient, adjective
inclement, adjective
enclosure, noun
inclusion, noun
inclusive, adjective
incognito, adjective, noun
incommode, verb
incorrect, adjective
incorrupt, adjective
increment, noun, verb
incubator, noun
inculcate, verb
inculpate, verb
incumbent, adjective, noun
incunable, noun
incurable, adjective, noun
incurious, adjective
incurrent, adjective
incursion, noun
incurvate, verb, adjective
indecency, noun
indecorum, noun
indelible, adjective
indemnify, verb
indemnity, noun
indention, noun
indenture, noun, verb
indexical, adjective, noun
Indianism, noun
indicator, noun
indiction, noun
indigence, noun
indignant, adjective
indignity, noun
indigotin, noun
indispose, verb
indolence, noun
induction, noun
inductive, adjective
indulgent, adjective
inebriant, adjective, noun
inebriate, verb, noun, adjective
ineffable, adjective
inelastic, adjective
inelegant, adjective
inequable, adjective
infantile, adjective
infantine, adjective
infatuate, verb
infection, noun
infective, adjective
inference, noun
infertile, adjective
infirmary, noun
infirmity, noun
inflation, noun
influence, noun, verb
influenza, noun
informant, noun
infuriate, verb
infusible, adjective
infusoria, pl.
ingenious, adjective
ingenuity, noun
ingenuous, adjective
inglenook, noun
ingrained, adjective
inhalator, noun
inheritor, noun
inhibitor, noun
initially, adverb
initiator, noun
injection, noun
injurious, adjective
injustice, noun
innermost, adjective
innervate, verb
innkeeper, noun
innocence, noun
innocuous, adjective
innovator, noun
inoculant, noun
inoculate, verb
inoculate, verb
inodorous, adjective
inorganic, adjective
inotropic, adjective
inpatient, noun
inquiline, noun
inquiring, adjective
insatiate, adjective
insectile, adjective
inselberg, noun
insensate, adjective
insertion, noun
insidious, adjective
insincere, adjective
insinuate, verb
insistent, adjective
insolence, noun
insoluble, adjective
insolvent, adjective, noun
inspector, noun
inspiring, adjective
instanter, adverb
instantly, adverb
instigate, verb
institute, noun, verb
insulator, noun
insulting, adjective
insurance, noun
insurgent, adjective, noun
inswinger, noun
integrate, verb
integrand, noun
integrant, adjective, noun
integrity, noun
intellect, noun
intendant, noun
intending, adjective
intensify, verb
intension, noun
intensity, noun
intensive, adjective, noun
intention, noun
interbank, adjective
intercede, verb
intercept, verb, noun
intercity, adjective
intercrop, verb, noun
interdict, noun, verb
interface, noun, verb
interfere, verb
interflow, verb
interfuse, verb
intergrow, verb
interject, verb
interknit, verb
interlace, verb
interlard, verb
interleaf, noun
interline, verb
interlink, verb
interlock, verb, noun
interlude, noun
interment, noun
intermesh, verb
internist, noun
internode, noun
interplay, noun
interpole, noun
interpose, verb
interpret, verb
interrupt, verb
intersect, verb
intervene, verb
interview, noun, verb
interwork, verb
interwind, verb
interwind, verb
intestate, adjective, noun
intestine, noun
intricacy, noun
intricate, adjective
intrigant, noun
intrinsic, adjective
introduce, verb
introvert, noun, adjective
intrusion, noun
intrusive, adjective
intuition, noun
intuitive, adjective
intumesce, verb
inunction, noun
invariant, adjective, noun
invective, noun
invention, noun
inventive, adjective
inventory, noun, verb
Inverness
inversion, noun
invertase, noun
invidious, adjective
inviolate, adjective
invisible, adjective, noun
involucre, noun
involuted, adjective
enwreathe, verb
inwrought, adjective
iodometry, noun
irascible, adjective
ironbound, adjective
Ironsides, noun
ironstone, noun
ironworks, noun
Iroquoian, noun, adjective
irradiant, adjective
irradiate, verb
irregular, adjective, noun
irritable, adjective
irritated, adjective
Irvingite, noun
isagogics, pl.
isallobar, noun
isinglass, noun
isobutane, noun
isoclinal, adjective
isocratic, adjective
isolating, adjective
isolation, noun
isomerous, adjective
isometric, adjective
isopropyl, noun
isopycnic, adjective
isosceles, adjective
isosmotic, adjective
isotropic, adjective
ispaghula, noun
Israelite, noun, adjective
italicize, verb
iteration, noun
iterative, adjective
itinerant, adjective, noun
itinerary, noun
itinerate, verb
icebreaker, noun
ichthyosis, noun
iconoclasm, noun
iconoclast, noun
iconolatry, noun
idealistic, adjective
idempotent, adjective, noun
identifier, noun
idiopathic, adjective
idolatrous, adjective
illiteracy, noun
illiterate, adjective, noun
illuminant, noun, adjective
illuminate, verb
illuminati, pl.
illuminati, pl.
illustrate, verb
imaginable, adjective
immaculate, adjective
immaterial, adjective
immaturity, noun
immemorial, adjective
immiscible, adjective
immobilize, verb
immoderate, adjective
immoralism, noun
immorality, noun
immortelle, noun
immunology, noun
impairment, noun
impalpable, adjective
impanation, noun
impassable, adjective
impassible, adjective
impatience, noun
impeccable, adjective
impediment, noun
impenitent, adjective
imperative, adjective, noun
imperatrix, noun
impersonal, adjective
impervious, adjective
implacable, adjective
implicitly, adverb
importance, noun
imposition, noun
impossible, adjective
impossibly, adverb
impoverish, verb
impregnate, verb
impresario, noun
impression, noun
impressive, adjective
imprimatur, noun
improbable, adjective
impudicity, noun
impuissant, adjective
inaccuracy, noun
inaccurate, adjective
inactivate, verb
inactivity, noun
inadequacy, noun
inadequate, adjective
inapparent, adjective
inapposite, adjective
inarguable, adjective
inartistic, adjective
inaugurate, verb
incandesce, verb
incapacity, noun
incautious, adjective
incendiary, adjective, noun
incestuous, adjective
inchoative, adjective, noun
incidental, adjective, noun
incinerate, verb
incitement, noun
incivility, noun
incoherent, adjective
incohesion, noun
incomplete, adjective
inconstant, adjective
incrassate, adjective
incredible, adjective
incredibly, adverb
incubation, noun
incumbency, noun
incunabula, noun
indecision, noun
indecisive, adjective
indecorous, adjective
indefinite, adjective
indelicacy, noun
indelicate, adjective
indication, noun
indicative, adjective, noun
indicatory, adjective
indicatrix, noun
indicolite, noun
indictable, adjective
indictment, noun
indigenous, adjective
indirectly, adverb
indiscreet, adjective
indiscrete, adjective
indisposed, adjective
indistinct, adjective
individual, adjective, noun
Indonesian, adjective, noun
inducement, noun
inductance, noun
indulgence, noun
indumentum, noun
industrial, adjective, noun
ineducable, adjective
ineligible, adjective
ineludible, adjective
inequality, noun
inevitable, adjective, noun
inevitably, adverb
inexistent, adjective
inexorable, adjective
inexpiable, adjective
inexplicit, adjective
infallible, adjective
infarction, noun
infeasible, adjective
infectious, adjective
infelicity, noun
infibulate, verb
infidelity, noun
infighting, noun
infiltrate, verb, noun
infinitive, noun, adjective
infinitude, noun
infirmarer, noun
inflatable, adjective, noun
inflection, noun
inflexible, adjective
infliction, noun
infraction, noun
infrarenal, adjective
infrequent, adjective
ingeminate, verb
inglorious, adjective
ingratiate, verb
ingredient, noun
ingressive, adjective, noun
inhabitant, noun
inhalation, noun
inharmonic, adjective
inhibition, noun
inhumanity, noun
inimitable, adjective
iniquitous, adjective
initialize, verb
initiation, noun
initiative, noun
injunction, noun
innominate, adjective
innovation, noun
innovative, adjective
inoperable, adjective
inordinate, adjective
inosculate, verb
inquietude, noun
inquisitor, noun
insanitary, adjective
insatiable, adjective
insecurity, noun
inseminate, verb
insensible, adjective
insentient, adjective
insightful, adjective
insistence, noun
insobriety, noun
insolation, noun
insolvable, adjective
insolvency, noun
insouciant, adjective
inspection, noun
inspissate, verb
instigator, noun
institutor, noun
instructor, noun
instrument, noun, verb
insufflate, verb
insularity, noun
insulation, noun
intangible, adjective, noun
integrator, noun
integument, noun
intendment, noun
interbreed, verb
interclass, adjective
intercross, verb, noun
interested, adjective
interfluve, noun
intergrade, verb, noun
interleave, verb
interloper, noun
intermarry, verb
intermezzo, noun
interphase, noun
interplant, verb
interspace, noun, verb
interstate, adjective, noun
interstice, noun
intertidal, adjective
intertrigo, noun
intertwine, verb
intertwist, verb
interweave, verb
intestinal, adjective
intimation, noun
intimidate, verb
intinction, noun
intolerant, adjective
intonation, noun
intoxicant, noun
intoxicate, verb
intramural, adjective
intriguing, adjective
introspect, verb
inundation, noun
invaginate, verb
invalidate, verb
invalidity, noun
invaluable, adjective
invariable, adjective
invariably, adverb
inventress, noun
inveracity, noun
investment, noun
inveterate, adjective
invigilate, verb
invigorate, verb
invincible, adjective
inviolable, adjective
invitation, noun
invitatory, adjective
invocation, noun
involatile, adjective
involution, noun
inwardness, noun
ionosphere, noun
iridaceous, adjective
iridectomy, noun
iridescent, adjective
Irishwoman, noun
ironically, adverb
ironmaster, noun
ironmonger, noun
irradiance, noun
irrational, adjective, noun
irrelative, adjective
irrelevant, adjective
irresolute, adjective
irreverent, adjective
irritating, adjective
irritation, noun
isentropic, adjective
isocyanate, noun
isocyanide, noun
isodynamic, adjective
isoflavone, noun
isoleucine, noun
isomorphic, adjective
isoseismal, adjective
Italianate, adjective
Italianism, noun
ichnography, noun
ichthyolite, noun
ichthyology, noun
ichthyornis, noun
ichthyosaur, noun
iconography, noun
iconostasis, noun
icosahedron, noun
idiographic, adjective
ignominious, adjective
illimitable, adjective
illuminance, noun
illusionism, noun
illusionist, noun
illustrator, noun
illustrious, adjective
illuviation, noun
imagination, noun
imaginative, adjective
immediately, adverb, conjunction
immedicable, adjective
immigration, noun
immitigable, adjective
immortality, noun
immortalize, verb
immunogenic, adjective
impassioned, adjective
impecunious, adjective
impedimenta, pl.
imperforate, adjective
imperialist, adjective, noun
imperialism, noun
imperialism, noun
imperialize, verb
impermanent, adjective
impermeable, adjective
impersonate, verb
impertinent, adjective
implausible, adjective
implication, noun
importantly, adverb
importunate, adjective
impractical, adjective
imprecation, noun
impregnable, adjective
impropriate, verb
impropriety, noun
improvement, noun
improvident, adjective
inadvertent, adjective
inadvisable, adjective
inalienable, adjective
inalterable, adjective
inappetence, noun
inattention, noun
inattentive, adjective
inauthentic, noun
incantation, noun
incarcerate, verb
incarnadine, noun, adjective, verb
incarnation, noun
incertitude, noun
incessantly, adverb
incinerator, noun
inclination, noun
incognizant, adjective
incompetent, adjective, noun
incongruent, adjective
incongruity, noun
incongruous, adjective
inconsonant, adjective
incontinent, adjective
incorporate, verb, adjective
incorporeal, adjective
incredulity, noun
incredulous, adjective
incriminate, verb
indefinable, adjective
indehiscent, adjective
indentation, noun
independent, adjective, noun
indifferent, adjective
indigestion, noun
indignation, noun
indirection, noun
individuate, verb
indivisible, adjective
indomitable, adjective
indubitable, adjective
industrious, adjective
inebriation, noun
ineffective, adjective
ineffectual, adjective
inefficient, adjective
ineluctable, adjective
inequitable, adjective
inequivalve, adjective
inescapable, adjective
inessential, adjective, noun
inestimable, adjective
inexcusable, adjective
inexpedient, adjective
inexpensive, adjective
infangthief, noun
infanticide, noun
infantilism, noun
infantryman, noun
infatuation, noun
inferiority, noun
infeudation, noun
inflammable, adjective
influential, adjective, noun
informality, noun
information, noun
informative, adjective
informatory, adjective
infrangible, adjective
infuriating, adjective
ingratitude, noun
ingurgitate, verb
inhabitable, adjective
inhabitancy, noun
inheritable, adjective
inheritance, noun
injudicious, adjective
innumerable, adjective
innutrition, noun
inoculation, noun
inoffensive, adjective
inoperative, adjective
inopportune, adjective
inquisition, noun
inquisitive, adjective
inscription, noun
inscrutable, adjective
insectarium, noun
insecticide, noun
Insectivora
insectivore, noun
insensitive, adjective
inseparable, adjective, noun
insinuation, noun
insouciance, noun
inspiration, noun
inspiratory, adjective
inspissator, noun
instability, noun
installment, noun
instigation, noun
instinctive, adjective
institution, noun
instruction, noun
instructive, adjective
insufflator, noun
insuperable, adjective
intagliated, adjective
integration, noun
integrative, adjective
intelligent, adjective
intemperate, adjective
intensifier, noun
intentional, adjective
interaction, noun
interactive, adjective
interagency, adjective
interallied, adjective
interatomic, adjective
intercalary, adjective
intercalate, verb
intercensal, adjective
interceptor, noun
intercessor, noun
interchange, verb, noun
intercooler, noun
intercostal, adjective, noun
intercourse, noun
intercrural, adjective
interdental, adjective, noun
interdictor, noun
interesting, adjective
interfacial, adjective
interfering, adjective
intergrowth, noun
interiority, noun
interlinear, adjective
interlingua, noun
interlining, noun
intermedium, noun
intermingle, verb
internalize, verb
internecine, adjective
interpolate, verb
interpreter, noun
interracial, adjective
interregnum, noun
interrelate, verb
interrogate, verb
interrupted, adjective
interrupter, noun
interseptal, adjective
intersexual, adjective
intersperse, verb
interspinal, adjective
intertribal, adjective
intolerable, adjective
intolerance, noun
intractable, adjective
intradermal, adjective
intrathecal, adjective
intravenous, adjective
introverted, adjective
intumescent, adjective
investigate, verb
investiture, noun
involuntary, adjective
involvement, noun
ipsilateral, adjective
irradiation, noun
irrecusable, adjective
irredentist, noun
irreducible, adjective
irreflexive, adjective
irrefutable, adjective
irrelevance, noun
irreligious, adjective
irremovable, adjective
irreparable, adjective
irresoluble, adjective
irreverence, noun
irrevocable, adjective
isethionate, noun
isobutylene, noun
isochronous, adjective
isoelectric, adjective
isogeotherm, noun
ithyphallic, adjective
iconoclastic, adjective
identifiable, adjective
idiosyncrasy, noun
illegitimate, adjective, noun
illumination, noun
illustration, noun
illustrative, adjective
immeasurable, adjective
immoderation, noun
impenetrable, adjective
imperceptive, adjective
impercipient, adjective
imperfection, noun
imperfective, adjective, noun
imperishable, adjective
impertinence, noun
implantation, noun
imponderable, noun, adjective
imprisonment, noun
impropriator, noun
inaccessible, adjective
inadmissible, adjective
inapplicable, adjective
inarticulate, adjective
inauguration, noun
inauspicious, adjective
incalculable, adjective
incandescent, adjective
incapacitate, verb
incidentally, adverb
inclinometer, noun
incommodious, adjective
incommutable, adjective
incomparable, adjective
incomparably, adverb
incompatible, adjective
incompetence, noun
incompletion, noun
incomputable, adjective
inconclusive, adjective
inconsequent, adjective
inconsistent, adjective
inconsolable, adjective
inconvenient, adjective
incorporated, adjective
incorrigible, adjective, noun
increasingly, adverb
encrustation, noun
indeclinable, adjective
indefeasible, adjective
indefectible, adjective
indefensible, adjective
indefinitely, adverb
independence, noun
independency, noun
indifference, noun
indigestible, adjective
indiscipline, noun
indiscretion, noun
indisputable, adjective
indissoluble, adjective
individually, adverb
indoctrinate, verb
ineffaceable, adjective
ineliminable, adjective
ineradicable, adjective
inescutcheon, noun
inexperience, noun
inexplicable, adjective
inexpressive, adjective
inexpugnable, adjective
inextensible, adjective
inextricable, adjective
infelicitous, adjective
infibulation, noun
inflammation, noun
inflammatory, adjective
inflationary, adjective
infringement, noun
infundibulum, noun
ingratiating, adjective
ingravescent, adjective
inharmonious, adjective
inhospitable, adjective
innutritious, adjective
inobservance, noun
insalubrious, adjective
inspectorate, noun
installation, noun
instauration, noun
instructress, noun
instrumental, adjective, noun
insufferable, adjective
insufficient, adjective
insurrection, noun
intellection, noun
intellectual, adjective, noun
intelligence, noun
intelligible, adjective
intemperance, noun
interception, noun
intercession, noun
interconnect, verb, noun
interconvert, verb
intercurrent, adjective
interdigital, adjective
interference, noun
interglacial, adjective, noun
interjection, noun
interlineate, verb
interlingual, adjective
interlobular, adjective
interlocutor, noun
intermediary, noun, adjective
intermediate, adjective, noun, verb
interminable, adjective
intermission, noun
intermittent, adjective
internuclear, adjective
internuncial, adjective
interoceanic, adjective
interoceptor, noun
interosseous, adjective
interpellate, verb
interpleader, noun
interpolator, noun
interruption, noun
intersection, noun
intersession, noun
interstadial, adjective, noun
interstellar, adjective
interstitial, adjective
intervention, noun
intervocalic, adjective
intimidation, noun
intoxicating, adjective
intoxication, noun
intracranial, adjective
intransigent, adjective, noun
intransitive, adjective, noun
introduction, noun
introductory, adjective
introjection, noun
intromission, noun
intuitionism, noun
invagination, noun
invertebrate, noun, adjective
investigable, adjective
investigator, noun
invigorating, adjective
invitational, adjective, noun
invulnerable, adjective
irrebuttable, adjective
irredeemable, adjective
irreformable, adjective
irrefragable, adjective
irregardless, adjective
irregularity, noun
irremediable, adjective
irremissible, adjective
irresistible, adjective
irresolution, noun
irresolvable, adjective
irrespective, adjective
irresponsive, adjective
irreversible, adjective
irritability, noun
irrotational, adjective
isochromatic, adjective
isodiametric, adjective
isolationism, noun
idiosyncratic, adjective
immaterialism, noun
imperceptible, adjective
impermissible, adjective
impersonation, noun
imperturbable, adjective
impossibilism, noun
impossibility, noun
impracticable, adjective
Impressionism, noun
impressionist, noun
improbability, noun
improvisation, noun
inadvertently, adverb
inappreciable, adjective
inappropriate, adjective
incarceration, noun
incombustible, adjective
incommunicado, adjective
incompletable, adjective
inconceivable, adjective
inconsecutive, adjective
inconsiderate, adjective
inconsistency, noun
inconspicuous, adjective
incontestable, adjective
inconvenience, noun, verb
inconvertible, adjective
incorporative, adjective
incorruptible, adjective
indefatigable, adjective
independently, adverb
indescribable, adjective
indeterminate, adjective
indeterminism, noun
indiscernible, adjective
indispensable, adjective
indisposition, noun
indissociable, adjective
indistinctive, adjective
individualism, noun
individuality, noun
individualize, verb
industrialism, noun
industrialist, noun
industrialize, verb
inefficacious, adjective
inexhaustible, adjective
inexperienced, adjective
inexpressible, adjective
infallibility, noun
infinitesimal, adjective, noun
inflorescence, noun
inhomogeneous, adjective
inquisitorial, adjective
insectivorous, adjective
insensibility, noun
insignificant, adjective
inspirational, adjective
instantaneity, noun
instantaneous, adjective
institutional, adjective
insubordinate, adjective
insubstantial, adjective
insufficiency, noun
insupportable, adjective
insusceptible, adjective
intelligencer, noun
intentionally, adverb
intercellular, adjective
intercolonial, adjective
intercultural, adjective
interdigitate, verb
interlanguage, noun
interlocutory, adjective
intermarriage, noun
international, adjective, noun
interoceptive, adjective
interpersonal, adjective
interposition, noun
interrogation, noun
interrogative, adjective, noun
interrogatory, adjective, noun
interspecific, adjective
intracellular, adjective
intramuscular, adjective
intraspecific, adjective
intravascular, adjective
introgression, noun
introspection, noun
introspective, adjective
inventiveness, noun
investigation, noun
investigative, adjective
iontophoresis, noun
irrationalism, noun
irreclaimable, adjective
irrecoverable, adjective
irrefrangible, adjective
irreplaceable, adjective
irrepressible, adjective
irresponsible, adjective
irretrievable, adjective
ischiorrhogic, adjective
isoelectronic, adjective
iatrochemistry, noun
ichthyophagous, adjective
identification, noun
implementation, noun
impressionable, adjective
inappreciative, adjective
incommensurate, adjective
incommunicable, adjective
incompressible, adjective
inconsiderable, adjective
indecipherable, adjective
indecomposable, adjective
indemonstrable, adjective
indestructible, adjective
indeterminable, adjective
indifferentism, noun
indiscriminate, adjective
infralapsarian, noun, adjective
infrastructure, noun
insurmountable, adjective
intelligentsia, noun
intentionalism, noun
intentionality, noun
interactionism, noun
interarticular, adjective
intercommunion, noun
intercommunity, adjective
interdependent, adjective
interferometer, noun
intermolecular, adjective
interpenetrate, verb
interplanetary, adjective
interpretation, noun
intersegmental, adjective
intervertebral, adjective
intramolecular, adjective
irreconcilable, adjective, noun
irreproachable, adjective
irreproducible, adjective
isothiocyanate, noun
immunochemistry, noun
imprescriptible, adjective
impressionistic, adjective
incommensurable, adjective, noun
incommunicative, adjective
incomprehension, noun
inconsequential, adjective
indetermination, noun
individualistic, adjective
instrumentalism, noun
instrumentalist, noun, adjective
instrumentality, noun
instrumentation, noun
insubordination, noun
intellectualism, noun
intellectualize, verb
intelligibility, noun
intensification, noun
interchangeable, adjective
intercollegiate, adjective
interprovincial, adjective, noun
intersubjective, adjective
interventionist, adjective, noun
intussusception, noun
incomprehensible, adjective
incontrovertible, adjective
indiscriminately, adverb
indiscriminating, adjective
inextinguishable, adjective
institutionalize, verb
intercommunicate, verb
intercontinental, adjective
intercorrelation, noun
internationalism, noun
internationalize, verb
isoagglutination, noun
indistinguishable, adjective
intercolumniation, noun
interdepartmental, adjective
intergovernmental, adjective
interrelationship, noun
intercommunication, noun
interdenominational, adjective
