V, noun
v., abbreviation, symb.
van, noun
vas, noun
vat, noun, verb
vee, noun
vet, noun, verb
vex, verb
via, preposition
vie, verb
vim, noun
VIP, noun
VOD, abbreviation
voe, noun
vow, noun, verb
vug, noun
vail, verb
vain, adjective
vair, noun
vale, noun
vamp, noun, verb
Van,
vane, noun
vang, noun
vara, noun
vary, verb
Vasa
vase, noun
vast, adjective, noun
veal, noun
Veda, noun
veep, noun
veer, verb, noun
Vega
veil, noun, verb
vein, noun
vela
veld, noun
vend, verb
vent, noun, verb
verb, noun
vert, noun
very, adverb, adjective
vest, noun, verb
veto, noun, verb
vial, noun
vice, noun
view, noun, verb
viga, noun
Vila
vile, adjective
vill, noun
vino, noun
vint, verb
vine, noun
viol, noun
visa, noun
vise, noun
vita, noun
viva, noun, verb
vlei, noun
void, adjective, noun, verb
vole, noun
volt, noun
vote, noun, verb
vuln, verb
vagal, adjective
vague, adjective
vagus, noun
vairy, adjective
vajra, noun
vakil, noun
valet, noun, verb
valid, adjective
valor, noun
valse, noun
value, noun, verb
valve, noun
Vanda
Vanir, noun
vapid, adjective
vapor, noun, verb
vardo, noun
varec, noun
vari-, comb.
varix, noun
varna, noun
varus, noun
varve, noun
vatic, adjective
vault, noun, verb
vaunt, verb, noun
Vedda, noun
Vedic, adjective, noun
veery, noun
velar, adjective, noun
velum, noun
venal, adjective
venom, noun
venue, noun
Venus
verge, noun, verb
verse, noun, verb
verso, noun
verst, noun
verve, noun
Vesta
vesta, noun
vetch, noun
vexed, adjective
viand, noun
vicar, noun
video, noun, verb
vigil, noun
vigor, noun
villa, noun
veena, noun
vinyl, noun
viola, noun
viper, noun
viral, adjective, noun
vireo, noun
virga, noun
Virgo
virtu, noun
virus, noun
visit, verb, noun
visor, noun
vista, noun
vital, adjective, noun
vitta, noun
vivid, adjective
vixen, noun
Vlach, noun, adjective
vocal, adjective, noun
vodka, noun
vogue, noun, adjective, verb
voice, noun, verb
voile, noun
volar, adjective
volet, noun
Volta
volva, noun
vomer, noun
vomit, verb, noun
voter, noun
vouch, verb
vowel, noun
vulva, noun
vying
vacant, adjective
vacate, verb
vacuum, noun, verb
vadose, adjective
vagary, noun
vagina, noun
veleta, noun
valgus, noun
valine, noun
valise, noun
valley, noun
vallum, noun
Valois
valued, adjective
valuer, noun
valuta, noun
vandal, noun
vanish, verb
vanity, noun
varied, adjective
varlet, noun
Varuna
vassal, noun
VATman, noun
vealer, noun
vector, noun, verb
veined, adjective
vellum, noun
velour, noun
velure, noun, verb
velvet, noun
vendee, noun
vendor, noun
vendue, noun
veneer, noun, verb
venery, noun
venial, adjective
Venice
Venite, noun
vennel, noun
venous, adjective
venter, noun
ventil, noun
venule, noun
verbal, adjective, noun
verdin, noun
verger, noun
verify, verb
verily, adverb
vérité, noun
verity, noun
vermis, noun
vermin, noun
vernal, adjective
Verona
versal, adjective, noun
versed, adjective
verset, noun
versus, preposition
vertex, noun
vervet, noun
vesper, noun
vespid, noun
vessel, noun
vestal, adjective, noun
vestee, noun
vestry, noun
viable, adjective
vibrio, noun
victim, noun
victor, noun
vicuña, noun
vielle, noun
Vienna
viewer, noun
vihara, noun
Viking, noun, adjective
vilify, verb
-ville, comb.
villus, noun
vinery, noun
vinous, adjective
violet, noun, adjective
violin, noun
virago, noun
virgin, noun, adjective
virile, adjective
virtue, noun
visage, noun
viscid, adjective
viscus
Vishnu
vision, noun, verb
visual, adjective, noun
vivers, plural
vivify, verb
vizard, noun
vizier, noun
voided, adjective
Volans
volant, adjective
volley, noun, verb
volume, noun
volute, noun, adjective
voodoo, noun, verb
vortex, noun
votary, noun
votive, adjective, noun
voyage, noun, verb
voyeur, noun
Vulcan
vulgar, adjective
vacancy, noun
vaccine, noun
vacuity, noun
vacuole, noun
vacuous, adjective
vagrant, noun, adjective
vaguely, adverb
valance, noun
valence, noun
valency, noun
-valent, comb.
valeric, noun
valiant, adjective
valonia, noun
valvate, adjective
vamoose, verb
vampire, noun
Vandyke, noun, adjective
vanilla, noun, adjective
vantage, noun
variant, noun
variate, noun
varices
variety, noun
variola, noun
various, adjective, det.
varnish, noun, verb
varsity, noun
Vatican
Vaudois, adjective, noun
Vedanta, noun
vedette, noun
vegetal, adjective
vehicle, noun
veiling, noun
veining, noun
veinous, adjective
velamen, noun
veliger, noun
vendace, noun
venison, noun
ventail, noun
Ventose, noun
ventral, adjective
venture, noun, verb
veranda, noun
verbena, noun
verbose, adjective
verdant, adjective
verdict, noun
Verdun,
verdure, noun
verglas, noun
vermeil, noun
vermian, adjective
Vermont
vernier, noun
veronal, noun
verruca, noun
versant, noun
versify, verb
versine, noun
version, noun, verb
vertigo, noun
vervain, noun
vesical, adjective
vesicle, noun
vespers, noun
vespine, adjective
vestige, noun
vesting, noun
vesture, noun
veteran, noun
vetiver, noun
viaduct, noun
vibrant, adjective
vibrate, verb
vibrato, noun
viceroy, noun
vicinal, adjective
vicious, adjective
victory, noun
victrix, noun
victual, noun, verb
vedette, noun
vihuela, noun
vilayet, noun
village, noun
villain, noun
villein, noun
villous, adjective
vinegar, noun
Vinland
vintage, noun, adjective
vintner, noun
violate, verb
violent, adjective
violist, noun
violone, noun
virelay, noun
viremia, noun
virgate, noun
virgule, noun
virtual, adjective
viscera, pl.
viscose, noun
viscous, adjective
visible, adjective
visitor, noun
vitamin, noun
vitiate, verb
vitrify, verb
vitrine, noun
vitriol, noun
vocable, noun
vocalic, adjective
Volapük, noun
volcano, noun
voltage, noun
voltaic, adjective
voluble, adjective
vomitus, noun
voucher, noun
Voyager
Vulgate, noun
vulpine, adjective
vulture, noun
vacation, noun, verb
vaccinia, noun
vagabond, noun, adjective, verb
vagotomy, noun
vagrancy, noun
Valencia
valerian, noun
Valhalla
validate, verb
validity, noun
Valkyrie, noun
valorize, verb
valuable, adjective, noun
valuator, noun
valvular, adjective
vambrace, noun
vamplate, noun
vanadate, noun
vanadium, noun
vanguard, noun
vanillin, noun
vanquish, verb
vapourer, noun
vaporize, verb
variable, adjective, noun
variance, noun
varicose, adjective
varietal, adjective, noun
variform, adjective
variorum, adjective, noun
vascular, adjective
vasculum, noun
Vaseline, noun, verb
vaulting, noun
vavasory, noun
vegetate, verb
vehement, adjective
velarium, noun
velleity, noun
velocity, noun
venation, noun
vendetta, noun
venerate, verb
venereal, adjective
Venetian, adjective, noun
vengeful, adjective
venomous, adjective
venturer, noun
veracity, noun
veratrum, noun
verbatim, adverb
verbiage, noun
Verdelho, noun
verderer, noun
verditer, noun, adjective
vergence, noun
veristic, adjective
verjuice, noun
vermouth, noun
versicle, noun
vertebra, noun
vertical, adjective, noun
vesicant, adjective, noun
vesicate, verb
vesperal, adjective, noun
vespiary, noun
vestiary, adjective, noun
vestment, noun
Vesuvius
vexation, noun
vexillum, noun
viaticum, noun
vibrator, noun
vibrissa, noun
viburnum, noun
vicarage, noun
vicarial, adjective
vicinage, noun
vicinity, noun
Victoria
Victrola, noun
Vietminh, noun
viewless, adjective
vigilant, adjective
vigneron, noun
vignette, noun, verb
vigorous, adjective
vilipend, verb
villainy, noun
vincible, adjective
vinculum, noun
Vineland
vineyard, noun
vintager, noun
violence, noun
virginal, adjective, noun
Virginia
viridian, noun
virilism, noun
virility, noun
virology, noun
virtuoso, noun
virtuous, adjective
virulent, adjective
viscacha, noun
visceral, adjective
viscount, noun
Visigoth, noun
visitant, noun, adjective
visiting, adjective
vitalism, noun
vitality, noun
vitalize, verb
vitellin, noun
vitellus, noun
vitiligo, noun
vitreous, adjective
vivacity, noun
vivarium, noun
vivisect, verb
Vladimir
vocalise, noun
vocalism, noun
vocalist, noun
vocalize, verb
vocation, noun
vocative, adjective, noun
voiceful, adjective
voidance, noun
volatile, adjective, noun
volcanic, adjective
volition, noun
volplane, noun, verb
Volscian, noun, adjective
volution, noun
volvulus, noun
vomitory, adjective, noun
voussoir, noun
vowelize, verb
vulvitis, noun
vaccinate, verb
vacillate, verb
vagarious, adjective
vaginitis, noun
vagotonia, noun
vainglory, noun
Vaishnava, noun
valentine, noun
vallecula, noun
valuation, noun
valueless, adjective
vampirism, noun
vandalism, noun
vandalize, verb
vaporizer, noun
Varangian, noun
variation, noun
varicella, noun
varietist, noun
varioloid, adjective, noun
variously, adverb
vasectomy, noun
vasomotor, adjective
vasospasm, noun
vastation, noun
vastitude, noun
vegetable, noun, adjective
vehemence, noun
veinstone, noun
velodrome, noun
velveteen, noun
veneering, noun
venerable, adjective
Venezuela
vengeance, noun
ventiduct, noun
ventifact, noun
ventilate, verb
ventricle, noun
venturous, adjective
veracious, adjective
veratrine, noun
verbalism, noun
verbalize, verb
verbascum, noun
verbosity, noun
verdigris, noun
veridical, adjective
veritable, adjective
vermicide, noun
vermiform, adjective
vermifuge, noun
vermilion, noun
verminate, verb
vernation, noun
Veronese,
Veronica,
versatile, adjective
vestibule, noun
vestigial, adjective
vestiture, noun
vestryman, noun
vetchling, noun
vexatious, adjective
vibratile, adjective
vibration, noun
vibratory, adjective
vicariate, noun
vicarious, adjective
viceregal, adjective
vicereine, noun
victimize, verb
Victorian, adjective, noun
victualer, noun
viewpoint, noun
vigesimal, adjective
vigilance, noun
vigilante, noun
vinaceous, adjective
vindicate, verb
violation, noun
viperfish, noun
virescent, adjective
virginity, noun
virtually, adverb
viscosity, noun
viscounty, noun
visionary, adjective, noun
visualize, verb
vitelline, adjective
vitriform, adjective
vitriolic, adjective
vivacious, adjective
vivianite, noun
voiceless, adjective
voltmeter, noun
voluntary, adjective, noun
volunteer, noun, verb
voracious, adjective
Vorticist, noun
vouchsafe, verb
Vulcanian, adjective
volcanism, noun
volcanism, noun
vulcanite, noun
vulcanize, verb
vulgarian, noun
vulgarism, noun
vulgarize, verb
vulnerary, adjective, noun
Vulpecula
vaginismus, noun
Valentine,
valvulitis, noun
vanadinite, noun
varicocele, noun
variegated, adjective
variometer, noun
vasculitis, noun
vaticinate, verb
vaudeville, noun
vegetarian, noun, adjective
vegetation, noun
vegetative, adjective
velitation, noun
velocipede, noun
velvetleaf, noun
veneration, noun
ventilator, noun
ventricose, adjective
vermicelli, noun
vermicular, adjective
vernacular, noun, adjective
vertebrate, noun, adjective
vesiculate, verb, adjective
vespertine, adjective
vestibular, adjective
veterinary, adjective, noun
vibraculum, noun
vibraphone, noun
vicegerent, noun
victorious, adjective
Vietnamese, noun, adjective
vigorously, adverb
villainous, adjective
villanella, noun
villanelle, noun
villeinage, noun
Vincentian, noun
vindictive, adjective
violaceous, adjective
virtualize, verb
virtuosity, noun
viscometer, noun
visibility, noun
visitation, noun
visitorial, adjective
vitaminize, verb
vitrescent, adjective
vituperate, verb
viviparous, adjective
vocabulary, noun
vocational, adjective
vociferate, verb
vociferous, adjective
volatilize, verb
volleyball, noun
volubility, noun
volumetric, adjective
voluminous, adjective
voluptuary, noun, adjective
voluptuous, adjective
vorticella, noun
vulnerable, adjective
vacillating, adjective
valediction, noun
valedictory, adjective, noun
vanishingly, adverb
varicolored, adjective
vascularize, verb
vasculature, noun
vasopressor, adjective, noun
velocimeter, noun
venereology, noun
venesection, noun
ventilation, noun
ventilatory, adjective
ventriculus, noun
venturesome, adjective
vermiculate, adjective
vermiculite, noun
vertiginous, adjective
vesuvianite, noun
viceroyalty, noun
vichyssoise, noun
vicissitude, noun
vinaigrette, noun
vindicative, adjective
viniculture, noun
violoncello, noun
viridescent, adjective
viscountess, noun
viticulture, noun
vivisection, noun
volcanicity, noun
volcanology, noun
voluntarily, adverb
voluntarism, noun
volcanology, noun
vaginoplasty, noun
Valenciennes, noun
valetudinary, adjective
variationist, noun
vasodilation, noun
velarization, noun
venipuncture, noun
ventripotent, adjective
ventromedial, adjective
verification, noun
vermiculated, adjective
versicolored, adjective
verticillium, noun
vestimentary, adjective
veterinarian, noun
vilification, noun
vinification, noun
viscosimeter, noun
visitatorial, adjective
vituperation, noun
vituperative, adjective
voluntaryism, noun
volunteerism, noun
valedictorian, noun
ventriloquist, noun
ventrolateral, adjective
vernalization, noun
viscerotropic, adjective
valetudinarian, noun, adjective
verisimilitude, noun
vespertilionid, noun
vulvovaginitis, noun
Völkerwanderung, noun
vasoconstriction, noun
ventriculography, noun
