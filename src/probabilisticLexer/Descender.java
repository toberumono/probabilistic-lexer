package probabilisticLexer;

import java.util.regex.Matcher;

import probabilisticLexer.errors.LexerException;

//Descender class for the lexer
public final class Descender {
	final String open, close;
	private final Type type;
	private final Action action;
	
	public Descender(String open, String close, Type type, Action action) {
		this.open = open;
		this.close = close;
		type.setOpenClose(open, close);
		this.type = type;
		this.action = action;
	}
	
	/**
	 * Apply the <tt>Action</tt> associated with this <tt>Descender</tt>
	 * 
	 * @param match
	 * @param lexer
	 * @return the resulting value for a representative <tt>Token</tt>
	 * @throws LexerException
	 */
	final Token apply(Matcher match, Lexer lexer) throws LexerException {
		return action == null ? lexer.makeNewToken(lexer.lex(match.group()), null, Type.EMPTY, type) : action.action(match, lexer, type);
	}
	
	public final Type getType() {
		return type;
	}
}
