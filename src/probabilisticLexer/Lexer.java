package probabilisticLexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lipstone.joshua.customStructures.maps.EqualsMap;
import lipstone.joshua.customStructures.stacks.PairedStack;
import probabilisticLexer.errors.EmptyInputException;
import probabilisticLexer.errors.LexerException;
import probabilisticLexer.errors.UnbalancedDescenderException;
import probabilisticLexer.errors.UnrecognizedCharacterException;
import probabilisticLexer.helpers.Default;
import probabilisticLexer.helpers.Smoother;

public class Lexer {
	private final EqualsMap<String, Rule> rules;
	private final EqualsMap<String, Rule> extraRules;
	private final EqualsMap<String, Descender> descenders;
	private final HashMap<String, Type> types;
	private final PairedStack<Pair<String, Smoother>> smoothers;
	private final EqualsMap<String, Default> defaults;
	private Type[] allTypes = new Type[0];
	private final ArrayList<Pattern> ignores;
	private final Stack<DescentSet> descentStack;
	private boolean ignoreSpace;
	private String input, lastSeparator;
	private int head;
	private Token current, output;
	
	/**
	 * Implements add 1 smoothing
	 */
	public static final Smoother ADD_1_SMOOTHER = new Smoother() {
		
		@Override
		public void smooth(Collection<Type> types) {
			for (Type type : types) {
				for (Object value : type.getValues())
					type.incrementValue(value);
				for (String t : type.getTypes())
					type.incrementType(t);
			}
		}
		
		@Override
		public void unsmooth(Collection<Type> types) {
			for (Type type : types) {
				for (Object value : type.getValues())
					type.decrementValue(value);
				for (String t : type.getTypes())
					type.decrementType(t);
			}
		}
		
	};
	
	/**
	 * Basic constructor for a <tt>Lexer</tt>
	 */
	public Lexer() {
		rules = new EqualsMap<>();
		extraRules = new EqualsMap<>();
		descenders = new EqualsMap<>();
		types = new HashMap<>();
		smoothers = new PairedStack<>();
		defaults = new EqualsMap<>();
		ignores = new ArrayList<>();
		descentStack = new Stack<>();
		ignoreSpace = true;
		input = "";
		head = 0;
		current = makeNewToken(null, null, Type.EMPTY, Type.EMPTY);
		output = current;
	}
	
	/**
	 * Loads data into the active corpus. If the current data has been smoothed, it is automatically unsmoothed before
	 * loading the corpus and then re-smoothed after the new data has been loaded.
	 * 
	 * @param typeSeparator
	 *            the <tt>String</tt> that represents the character or characters that link a token to its type
	 * @param paths
	 * @throws LexerException
	 */
	public void loadCorpus(final String typeSeparator, String... paths) throws LexerException {
		lastSeparator = typeSeparator;
		unsmooth();
		HashMap<String, StringBuilder> newRules = new HashMap<>();
		Action a = makeNewDefaultAction(typeSeparator);
		for (String path : paths) {
			File taggedDirectory = new File(path);
			boolean directory = taggedDirectory.isDirectory();
			File[] directoryContents;
			if (taggedDirectory.isDirectory())
				directoryContents = taggedDirectory.listFiles();
			else
				directoryContents = new File[]{taggedDirectory};
			for (File file : directoryContents) {
				if (file.isDirectory()) { //Support for sub-directories
					loadCorpus(typeSeparator, file.getAbsolutePath());
					continue;
				}
				//Fixes incorrect loading of .DS_Store on Mac.
				if (directory && file.getName().startsWith("."))
					continue;
				try {
					Scanner scanner = new Scanner(file);
					while (scanner.hasNextLine()) {
						String line = scanner.nextLine();
						if (!line.contains(typeSeparator))
							continue;
						String last = "Empty";
						int previous = 0, nextSeparator;
						while ((nextSeparator = line.indexOf(typeSeparator, previous)) != -1) {
							String token = line.substring(previous, nextSeparator).trim();
							for (; previous < line.length() && (line.charAt(previous) != ' ' && line.charAt(previous) != '\n' && line.charAt(previous) != '\r' && line.charAt(previous) != '\t'); previous++);
							String t = line.substring(nextSeparator + 1, previous);
							previous++;
							if (t.contains(typeSeparator))
								continue;
							Type type = types.get(t);
							if (type == null) {
								type = makeNewDefaultType(t, typeSeparator);
								types.put(t, type);
							}
							Rule match = null;
							Matcher m = null;
							for (Rule rule : rules.getValues())
								if ((m = rule.getPattern().matcher(token)).matches() && (rule.getType() != null && rule.getType().equals(type))) {
									match = rule;
									break;
								}
							if (a != null && match == null) {
								m = Pattern.compile(token, Pattern.LITERAL).matcher(token);
								m.find();
							}
							if (!newRules.containsKey(t))
								newRules.put(t, new StringBuilder(2000));
							if (newRules.get(t).indexOf(token) == -1)
								newRules.get(t).append("|" + token); //Save the word into the partially constructed rule
							type.addItem(last, a == null ? token : a.action(m, this, type).getCar());
							last = t;
						}
					}
					System.out.println("Loaded: " + file);
					scanner.close();
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		//Convert the found words into rules
		for (String key : newRules.keySet()) {
			String other = "";
			if (!types.containsKey(key))
				types.put(key, makeNewDefaultType(key, typeSeparator));
			if (extraRules.containsKey(key)) {
				other = extraRules.get(key).getPattern().pattern();
				other = other.substring(3, other.length() - 1) + "|";
			}
			extraRules.put(key, new Rule("(" + other + newRules.get(key).toString().substring(1).replaceAll("\\\\(\\S)", "\\\\\\\\$1").replaceAll("([\\Q?.$\\E])", "\\\\$1") + ")", types.get(key), a));
		}
		smooth();
	}
	
	/**
	 * Determines if this <tt>Lexer</tt> has a type with the given name,
	 * 
	 * @param type
	 *            the name of the type to check for
	 * @return true if this <tt>Lexer</tt>
	 */
	public boolean hasType(String type) {
		for (Type t : types.values())
			if (t.equals(type))
				return true;
		return false;
	}
	
	/**
	 * Tokenizes a <tt>String</tt>
	 * 
	 * @param input
	 *            the <tt>String</tt> to tokenize
	 * @return the <tt>Token</tt>s in the <tt>String</tt>
	 * @throws LexerException
	 */
	public final Token lex(String input) throws LexerException {
		smooth();
		//Save state
		descentStack.push(new DescentSet(this.input, head, output));
		this.input = input;
		current = makeNewToken(null, null, Type.EMPTY, Type.EMPTY);
		output = current;
		head = 0;
		try {
			while (head < input.length())
				if (hasNext())
					current = current.append(getNextToken(true));
				else
					break;
		}
		catch (LexerException e) {
			descentStack.clear();
			throw e;
		}
		Token result = output;
		//Return to previous state
		this.input = descentStack.peek().getInput();
		head = descentStack.peek().getHead();
		output = descentStack.pop().getOutput();
		current = output.getLastToken();
		return result;
	}
	
	/**
	 * Gets the next token in the input without stepping this <tt>Lexer</tt> forward.
	 * 
	 * @return the next token in this <tt>Lexer</tt>'s input
	 * @throws LexerException
	 *             if no token was found
	 */
	public final Token getNextToken() throws LexerException {
		return getNextToken(false);
	}
	
	/**
	 * Finds the next token in this <tt>Lexer</tt>
	 * 
	 * @param step
	 *            if this is true, it steps this <tt>Lexer</tt>'s read-head forward. If it is false, the read-head will still
	 *            be stepped forward for any calls to the Action associated with the next <tt>Token</tt>, but will be
	 *            returned to its original position after those calls
	 * @return the next token in this <tt>Lexer</tt>'s input
	 * @throws LexerException
	 *             if no token was found
	 */
	public final Token getNextToken(boolean step) throws LexerException {
		skipIgnores();
		if (head >= input.length())
			throw new EmptyInputException();
		Descender d = null;
		for (Descender descender : descenders.getValues())
			if (input.length() - head >= descender.open.length() && input.startsWith(descender.open, head) && (d == null || descender.open.length() > d.open.length()))
				d = descender;
		if (d != null) {
			int close = getEndIndex(input, head, d.open, d.close);
			Matcher m = Pattern.compile("\\Q" + input.substring(head + d.open.length(), close) + "\\E").matcher(input);
			m.find(head);
			int oldHead = head;
			head = close + d.close.length();
			try {
				return d.apply(m, this);
			}
			catch (LexerException e) {
				throw e;
			}
			finally {
				if (!step) //If we aren't supposed to step, put the read-head back
					head = oldHead;
			}
		}
		ArrayList<Type> types = new ArrayList<Type>();
		Rule hit = null;
		Matcher m, match = null;
		//Basically, go through the rules and find the longest match.  When you find a longer match, clear the types and add the types from that match.
		//If you find a match of the same length, then add the types from that match, ensuring that there are no duplicates.
		for (Rule rule : rules.getValues()) {
			m = rule.getPattern().matcher(input);
			if (m.find(head) && m.group().length() != 0) {
				if (match == null || match.group().length() < m.group().length()) {
					types = new ArrayList<Type>();
					match = m;
					hit = rule;
					if (rule.getType() != null)
						types.add(rule.getType());
				}
				else if (match.group().length() == m.group().length() && rule.getType() != null && !types.contains(rule.getType()))
					types.add(rule.getType());
			}
		}
		//Do the same thing as above, but with the rules found in the corpus.
		for (Rule rule : extraRules.getValues()) {
			m = rule.getPattern().matcher(input);
			if (m.find(head) && m.group().length() != 0) {
				if (match == null || match.group().length() < m.group().length()) {
					types = new ArrayList<Type>();
					match = m;
					hit = rule;
					if (rule.getType() != null)
						types.add(rule.getType());
				}
				else if (match.group().length() == m.group().length() && rule.getType() != null && !types.contains(rule.getType()))
					types.add(rule.getType());
			}
		}
		if (hit != null) {
			int oldHead = head;
			head += match.group().length();
			try {
				return hit.apply(match, this, types.toArray(new Type[types.size()]));
			}
			catch (LexerException e) {
				throw e;
			}
			finally {
				if (!step) //If we aren't supposed to step, put the read-head back
					head = oldHead;
			}
		}
		if (input.charAt(head) == ' ') {
			head++;
			return getNextToken(step);
		}
		throw new UnrecognizedCharacterException(input, head);
	}
	
	private final void skipIgnores() {
		while (true) {
			if (ignoreSpace) //This is true if none of patterns start with spaces.
				while (head < input.length() && input.charAt(head) == ' ')
					head++;
			Matcher m = null, check;
			//Get the longest match from the read-head in the ignore patterns
			for (Pattern p : ignores)
				if ((check = p.matcher(input)).find(head) && (m == null || check.end() > m.end()))
					m = check;
			//If nothing matched starting at the read-head, break
			if (m == null)
				break;
			//Otherwise, skip it
			head = m.end();
		}
	}
	
	/**
	 * While this isn't necessarily a watertight check, it does cover the majority of rationally constructed patterns
	 * 
	 * @param regex
	 *            the pattern to check
	 * @return true if a match of this pattern could start with a space, otherwise false
	 */
	private final boolean startsWithSpace(String regex) {
		if (regex.startsWith("^"))
			regex = regex.substring(1);
		else if (regex.startsWith("\\G") || regex.startsWith("\\b") || regex.startsWith("\\B") || regex.startsWith("\\A"))
			regex = regex.substring(2);
		if (regex.charAt(0) == ' ' || regex.charAt(0) == '.' || (regex.charAt(0) == '\\' && regex.length() > 1 && regex.charAt(1) == ' '))
			return true;
		if (regex.charAt(0) == '[') {
			try {
				return regex.substring(0, getEndIndex(regex, 0, "[", "]")).contains(" ");
			}
			catch (UnbalancedDescenderException e) {/*Cannot occur because the pattern is valid*/}
		}
		if (regex.charAt(0) == '(') {
			try {
				for (String option : regex.substring(0, getEndIndex(regex, 0, "(", ")")).split("(?<!\\\\)\\|"))
					if (startsWithSpace(option))
						return true;
			}
			catch (UnbalancedDescenderException e) {/*Cannot occur because the pattern is valid*/}
			return false;
		}
		return false;
	}
	
	/**
	 * @return the output this lexer is currently generating.
	 */
	public final Token getPreviousToken() {
		return current;
	}
	
	/**
	 * @return the last character or set of characters used to denote the end of a token and the start of its type.
	 */
	public final String getLastSeparator() {
		return lastSeparator;
	}
	
	/**
	 * @return true if there is still untokenized input, otherwise false
	 */
	public final boolean hasNext() {
		skipIgnores();
		return head < input.length();
	}
	
	/**
	 * Adds a new rule
	 * 
	 * @param name
	 *            the name of the rule
	 * @param rule
	 *            the rule
	 */
	public final void addRule(String name, Rule rule) {
		if (ignoreSpace)
			ignoreSpace = !startsWithSpace(rule.getPattern().pattern().substring(2));
		rules.put(name, rule);
		if (rule.getType() != null)
			types.put(rule.getType().getName(), rule.getType());
	}
	
	/**
	 * Adds a new descender
	 * 
	 * @param name
	 *            the name of the descender
	 * @param descender
	 *            the descender
	 */
	public final void addDescender(String name, Descender descender) {
		if (ignoreSpace)
			ignoreSpace = !(descender.open.charAt(0) == ' ' || descender.close.charAt(0) == ' ');
		descenders.put(name, descender);
	}
	
	/**
	 * Tells the lexer to skip over the given regex <tt>String</tt>.
	 * 
	 * @param ignore
	 *            the <tt>Pattern</tt> to ignore as a regex <tt>String</tt>
	 */
	public final void ignore(String ignore) {
		ignores.add(Pattern.compile(ignore.startsWith("\\G") ? ignore : "\\G" + ignore));
	}
	
	/**
	 * @return the types that this lexer can find.
	 */
	public HashMap<String, Type> getTypes() {
		return types;
	}
	
	public final void addDefault(String name, Default d) {
		defaults.put(name, d);
	}
	
	/**
	 * Adds a new <tt>Smoother</tt> to this lexer.
	 * 
	 * @param name
	 *            the name of the <tt>Smoother</tt> to be added
	 * @param smoother
	 *            the <tt>Smoother</tt> to be added
	 */
	public final void addSmoother(String name, Smoother smoother) {
		smoothers.pushLeft(new Pair<String, Smoother>(name, smoother));
	}
	
	public final ArrayList<Smoother> removeSmoother(String name) {
		unsmooth();
		Stack<Pair<String, Smoother>> left = smoothers.getLeft();
		ArrayList<Smoother> smoothers = new ArrayList<Smoother>(2);
		for (int i = 0; i < left.size(); i++)
			if (left.get(i).getX().equals(name))
				smoothers.add(left.remove(i).getY());
		return smoothers;
	}
	
	private final void smooth() {
		while (smoothers.getSizeLeft() > 0)
			smoothers.shiftRight().getY().smooth(types.values());
	}
	
	private final void unsmooth() {
		while (smoothers.getSizeRight() > 0)
			smoothers.shiftLeft().getY().unsmooth(types.values());
	}
	
	/**
	 * Determines the default type of the car value of a token
	 * 
	 * @param car
	 *            the value to determine the type of
	 * @return the type of the value or null
	 */
	public final Type getDefaultType(Object car) {
		for (Default d : defaults.getValues()) {
			Type type = d.determineType(car);
			if (type != null)
				return type;
		}
		return null;
	}
	
	public Type[] getAllTypes() {
		if (allTypes.length != types.size())
			allTypes = types.values().toArray(new Type[types.values().size()]);
		return allTypes;
	}
	
	private int getEndIndex(String input, int start, String startSymbol, String endSymbol) throws UnbalancedDescenderException {
		int index = 0, parenthesis = 0;
		for (int i = start; i < input.length() - startSymbol.length() + 1 && i < input.length() - endSymbol.length() + 1; i++) {
			if (input.substring(i, i + startSymbol.length()).equals(startSymbol))
				parenthesis++;
			if (input.substring(i, i + endSymbol.length()).equals(endSymbol))
				parenthesis--;
			if (parenthesis == 0) {
				index = i;
				break;
			}
			if (input.charAt(i) == '\\') {
				i++;
				continue;
			}
		}
		if (parenthesis != 0)
			throw new UnbalancedDescenderException(input, start);
		return index;
	}
	
	/**
	 * Override this to change the base that this lexer uses for the default <tt>Type</tt>
	 * 
	 * @param type
	 *            the name for the type. <i>This must be used to name the type that this method returns</i>
	 * @param typeSeparator
	 *            the type separator that this lexer is using when this method is called
	 * @return a new instance of the default type that this lexer uses
	 */
	public Type makeNewDefaultType(String type, final String typeSeparator) {
		return new Type(type) {
			@Override
			public String valueToString(Object value) {
				return value.toString() + typeSeparator + this.getName() + " ";
			}
		};
	}
	
	/**
	 * Override this to change the base that this lexer uses for the default <tt>Action</tt>
	 * 
	 * @param typeSeparator
	 *            the type separator that this lexer is using when this method is called
	 * @return a new instance of the default type that this lexer uses
	 */
	public Action makeNewDefaultAction(final String typeSeparator) {
		return null;
	}
	
	/**
	 * Initializes a new terminating <tt>Token</tt>
	 * 
	 * @return a null <tt>Token</tt>
	 */
	public final Token makeNewToken() {
		return makeNewToken(null, null, Type.EMPTY, Type.EMPTY);
	}
	
	/**
	 * Override this to change the base that this lexer uses for <tt>Token</tt>s.
	 * 
	 * @param car
	 *            the car of the token
	 * @param cdr
	 *            the cdr of the token
	 * @param cdrType
	 *            the cdrType of the token
	 * @param carType
	 *            the carType of the token
	 * @return a new <tt>Token</tt> with the desired data and type or types
	 */
	public Token makeNewToken(Object car, Object cdr, Type cdrType, Type... carType) {
		return new Token(this, car, cdr, cdrType, carType);
	}
}
