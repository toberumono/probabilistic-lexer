package probabilisticLexer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import probabilisticLexer.errors.LexerException;

/**
 * A Rule for a {@link probabilisticLexer.Lexer Probabilistic Lexer}.
 * 
 * @author Joshua Lipstone
 */
public final class Rule {
	private final Pattern pattern;
	private final Type type;
	private final boolean hasType;
	private final Action action;
	
	/**
	 * Constructs a new <tt>Rule</tt> with the given data
	 * 
	 * @param pattern
	 *            a regex <tt>String</tt> that contains the <tt>Pattern</tt> for this <tt>Rule</tt> to use.
	 * @param type
	 *            the the type that <tt>Token</tt>s matched by this rule can have
	 * @param action
	 *            the <tt>Action</tt> to take on <tt>Token</tt>s matched by this rule
	 */
	public Rule(String pattern, Type type, Action action) {
		this.pattern = pattern.startsWith("\\G") ? Pattern.compile(pattern) : Pattern.compile("\\G" + pattern);
		this.action = action;
		this.type = type;
		hasType = type != null;
	}
	
	/**
	 * Constructs a new <tt>Rule</tt> with the given data
	 * 
	 * @param pattern
	 *            a regex <tt>String</tt> that contains the <tt>Pattern</tt> for this <tt>Rule</tt> to use.
	 * @param flags
	 *            the regex flags defined in {@link java.util.regex.Pattern Pattern}
	 * @param type
	 *            the the type that <tt>Token</tt>s matched by this rule can have
	 * @param action
	 *            the <tt>Action</tt> to take on <tt>Token</tt>s matched by this rule
	 */
	public Rule(String pattern, int flags, Type type, Action action) {
		this.pattern = pattern.startsWith("\\G") ? Pattern.compile(pattern, flags) : Pattern.compile("\\G" + pattern, flags);
		this.type = type;
		this.action = action;
		hasType = type != null;
	}

	/**
	 * Apply the <tt>Action</tt> associated with this <tt>Rule</tt>
	 * 
	 * @param match
	 *            the match that triggered this rule
	 * @param lexer
	 *            the <tt>Lexer</tt> that found the match
	 * @return the resulting value for a representative <tt>Token</tt>
	 * @throws LexerException
	 */
	protected final Token apply(Matcher match, Lexer lexer) throws LexerException {
		//Basically, if the Action is null, return a Token that contains the matched String as its car.  Otherwise, return the result of applying the Action to the matched String. 
		return action == null ? (hasType ? lexer.makeNewToken(match.group(), null, Type.EMPTY, type) : lexer.makeNewToken(match.group(), null, Type.EMPTY, lexer.getAllTypes())) :
				(hasType ? action.action(match, lexer, type) : action.action(match, lexer, lexer.getAllTypes()));
	}
	
	/**
	 * Apply the <tt>Action</tt> associated with this <tt>Rule</tt>
	 * 
	 * @param match
	 *            the match that triggered this rule
	 * @param lexer
	 *            the <tt>Lexer</tt> that found the match
	 * @param type
	 *            the <tt>Type</tt> or <tt>Type</tt>s that the resulting <tt>Token</tt> can have
	 * @return the resulting value for a representative <tt>Token</tt>
	 * @throws LexerException
	 */
	protected final Token apply(Matcher match, Lexer lexer, Type... type) throws LexerException {
		//Basically, if the Action is null, return a Token that contains the matched String as its car.  Otherwise, return the result of applying the Action to the matched String.
		return action == null ? (type.length > 0 ? lexer.makeNewToken(match.group(), null, Type.EMPTY, type) : lexer.makeNewToken(match.group(), null, Type.EMPTY, lexer.getAllTypes())) :
				(type.length > 0 ? action.action(match, lexer, type) : action.action(match, lexer, lexer.getAllTypes()));
	}
	
	/**
	 * @return the <tt>Type</tt> that tokens identified by this <tt>Rule</tt> can have
	 */
	public final Type getType() {
		return type;
	}
	
	/**
	 * @return the <tt>Pattern</tt> that this <tt>Rule</tt> matches
	 */
	public Pattern getPattern() {
		return pattern;
	}
}
