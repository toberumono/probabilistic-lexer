package probabilisticLexer;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Set;

public class Type {
	protected String name;
	protected String open, close;
	
	private final HashMap<String, Double> types;
	private final HashMap<Object, Double> values;
	private int valuesCount, typesCount;
	
	/**
	 * A pre-created type that <i>must</i> be used to denote empty tokens (e.g. the end of a list)
	 */
	public static final Type EMPTY = new Type("Empty") {
		@Override
		public String valueToString(Object value) {
			return "";
		}
		
		@Override
		public int compareValues(Object value1, Object value2) {
			return 0;
		}
		
		@Override
		protected Double probability(Object car, String previousType) {
			return probabilityType(previousType);
		}
	};
	
	/**
	 * A pre-created type that flags the <tt>Token</tt> as a descender point (e.g. parentheses)
	 */
	public static final Type TOKEN = new TokenType("Token", null, null);
	
	public Type(String name, String open, String close) {
		this.name = name;
		this.open = open;
		this.close = close;
		types = new HashMap<>();
		values = new HashMap<>();
		valuesCount = 0;
		typesCount = 1;
		types.put("<ukn>", 0.0);
	}
	
	public Type(String name) {
		this(name, null, null);
	}
	
	public final void setOpenClose(String open, String close) {
		this.open = open;
		this.close = close;
	}
	
	/**
	 * @return the name of this Type
	 */
	public final String getName() {
		return name;
	}
	
	/**
	 * @return whether this <tt>Type</tt> indicates a descender (its associated field is a subclass of Token)
	 */
	public final boolean marksDescender() {
		return open != null;
	}
	
	/**
	 * By default this simply forwards to the value's toString() method. However, this can be overridden for
	 * implementation-specific methods.
	 * 
	 * @param value
	 * @return the value as a <tt>String</tt>
	 */
	public String valueToString(Object value) {
		return (open != null ? open + value.toString() + close : value.toString()) + " ";
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public final boolean equals(Object o) {
		if (o instanceof Type)
			return ((Type) o).name.equals(name);
		if (o instanceof String)
			return ((String) o).equals(name);
		return false;
	}
	
	/**
	 * By default this forwards to the compareTo method of whatever object this <tt>Type</tt> denotes.</br> If the value type
	 * does not implement comparable, it returns 0.
	 * 
	 * @param value1
	 * @param value2
	 * @return the value of the implied value type's compareTo method if it implements <tt>Comparable</tt> otherwise 0.
	 */
	public int compareValues(Object value1, Object value2) {
		if (value1 instanceof Token)
			if (value2 instanceof Token)
				return ((Token) value1).compareTo((Token) value2);
			else
				return 1;
		else if (value2 instanceof Token)
			return -1;
		if (value1.getClass().isInstance(value2) && value1 instanceof Comparable)
			return ((Comparable<Object>) value1).compareTo(value2);
		return 0;
	}
	
	/**
	 * By default this just returns the object, which works for immutable objects like <tt>String</tt>, <tt>Integer</tt>,
	 * <tt>Double</tt>, etc or objects that implement the <tt>Cloneable</tt> interface. However, types that use any other
	 * object type should override this method.
	 * 
	 * @param value
	 *            the value to clone
	 * @return a clone of the passed object
	 */
	public Object clone(Object value) {
		if (value instanceof Token)
			return ((Token) value).clone();
		try {
			return value instanceof Cloneable ? value.getClass().getMethod("clone").invoke(value) : value;
		}
		catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException e) {
			return value;
		}
	}
	
	public void addItem(String type, Object value) {
		types.put(type, types.containsKey(type) ? types.get(type) + 1 : 1);
		values.put(value, values.containsKey(value) ? values.get(value) + 1 : 1);
		typesCount++;
		valuesCount++;
	}
	
	/**
	 * Equivalent to this <tt>Type</tt> appearing linked to the given value within the corpus
	 * 
	 * @param value
	 *            the value to increment
	 */
	public void incrementValue(Object value) {
		values.put(value, values.containsKey(value) ? values.get(value) + 1 : 1);
		valuesCount++;
	}
	
	/**
	 * Equivalent to this <tt>Type</tt> appearing immediately after the given type within the corpus
	 * 
	 * @param type
	 *            the type to increment
	 */
	public void incrementType(String type) {
		types.put(type, types.containsKey(type) ? types.get(type) + 1 : 1);
		typesCount++;
	}
	
	/**
	 * Equivalent to seeing the given value linked to this type one less time than was recorded before calling this method
	 * 
	 * @param value
	 *            the value to decrement
	 */
	public void decrementValue(Object value) {
		if (!values.containsKey(value))
			return;
		values.put(value, values.get(value) - 1);
		valuesCount--;
	}
	
	/**
	 * Equivalent to seeing the given type immediately before this type one less time than was recorded before calling this
	 * method
	 * 
	 * @param type
	 *            the type to decrement
	 */
	public void decrementType(String type) {
		if (!types.containsKey(type))
			return;
		types.put(type, types.get(type) - 1);
		typesCount--;
	}
	
	/**
	 * @return the list of car values that this type recognizes. <i>Do not remove any items from this <tt>Set</tt></i>
	 */
	public Set<Object> getValues() {
		return values.keySet();
	}
	
	/**
	 * @return the list of car types that this type recognizes. <i>Do not remove any items from this <tt>Set</tt></i>
	 */
	public Set<String> getTypes() {
		return types.keySet();
	}
	
	protected Double probability(Object car, String previousType) {
		return (values.containsKey(car) ? values.get(car) : 0) / valuesCount * probabilityType(previousType);
	}
	
	protected Double probabilityType(String previousType) {
		return (types.containsKey(previousType) ? types.get(previousType) : types.get("<ukn>")) / typesCount;
	}
}

class TokenType extends Type {
	
	public TokenType(String name, String open, String close) {
		super(name, open, close);
	}
	
	@Override
	public int compareValues(Object value1, Object value2) {
		if (value1 instanceof Token)
			if (value2 instanceof Token)
				return ((Token) value1).compareTo((Token) value2);
			else
				return 1;
		else if (value2 instanceof Token)
			return -1;
		return 0;
	}
	
	@Override
	public Object clone(Object value) {
		if (value instanceof Token)
			return ((Token) value).clone();
		return super.clone(value);
	}
}