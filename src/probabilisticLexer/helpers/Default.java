package probabilisticLexer.helpers;

import probabilisticLexer.Type;

public interface Default {
	
	/**
	 * Attempts to determine the correct type of car without external information
	 * 
	 * @param car
	 *            the value to determine the type of
	 * @return the appropriate type for car, or null if it cannot find one
	 */
	public Type determineType(Object car);
}
