package probabilisticLexer.helpers;

import java.util.Collection;

import probabilisticLexer.Type;

public interface Smoother {
	
	/**
	 * Smoothes the given data.</br> <i>smooth(unsmooth) on a smoothed <tt>Type</tt> must return the original <tt>Types</tt></i>
	 * 
	 * @param types
	 *            the <tt>Types</tt> to smooth
	 */
	public void smooth(Collection<Type> types);
	
	/**
	 * Unsmoothes the given data.</br> <i>unsmooth(smooth) on an unsmoothed <tt>Type</tt> must return the original <tt>Types</tt></i>
	 * 
	 * @param types
	 *            the <tt>Types</tt> to unsmooth
	 */
	
	public void unsmooth(Collection<Type> types);
}
