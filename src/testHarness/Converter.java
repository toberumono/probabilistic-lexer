package testHarness;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Comparator;

import lipstone.joshua.customStructures.lists.SortedList;

public class Converter {
	
	public static void main(String[] args) {
		char letter = 'A';
		for (int lettr = 0; lettr < 26; lettr++, letter = (char) (letter + 1)) {
			Comparator<String[]> comparator = new Comparator<String[]>() {
				
				@Override
				public int compare(String[] o1, String[] o2) {
					return o1[0].compareTo(o2[0]) != 0 ? (o1[0].length() > o2[0].length() ? 1 : -1) : o1[0].compareTo(o2[0]);
				}
			};
			BufferedReader br;
			try {
				br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("/Users/joshualipstone/Documents/Dictionary/" + letter + ".txt")), "utf-8"));
			}
			catch (FileNotFoundException | UnsupportedEncodingException e1) {
				e1.printStackTrace();
				return;
			}
			try {
				String line = br.readLine();
				
				SortedList<String[]> list = new SortedList<String[]>(comparator);
				while (line != null) {
					if (line.length() == 0 || !line.contains(" ")) {
						line = br.readLine();
						continue;
					}
					String[] data = line.split(", ");
					if (list.indexOf(data) == -1) {
						for (int i = 1; i < data.length; i++) {
							String part = data[i];
							if (part.equalsIgnoreCase("Coordinating conjunction"))
								data[i] = "CC";
							else if (part.equalsIgnoreCase("Cardinal number"))
								data[i] = "CD";
							else if (part.toLowerCase().startsWith("det"))
								data[i] = "DT";
							else if (data[0].equalsIgnoreCase("there") && part.equalsIgnoreCase("Existential"))
								data[i] = "EX";
							else if (part.equalsIgnoreCase("Preposition") || part.equalsIgnoreCase("Subordinating conjuction"))
								data[i] = "IN";
							else if (part.equalsIgnoreCase("Adjective"))
								data[i] = "JJ";
							else if (part.equalsIgnoreCase("Comparative adjective"))
								data[i] = "JJR";
							else if (part.equalsIgnoreCase("Superlative adjective"))
								data[i] = "JJS";
							else if (part.equalsIgnoreCase("List item marker"))
								data[i] = "LS";
							else if (part.equalsIgnoreCase("Modal"))
								data[i] = "MD";
							else if (part.toLowerCase().startsWith("Noun"))
								data[i] = "NN";
							else if (part.toLowerCase().startsWith("Proper noun"))
								data[i] = "NNP";
							else if (part.equalsIgnoreCase("Predeterminer"))
								data[i] = "PDT";
							else if (part.equalsIgnoreCase("Possesive ending"))
								data[i] = "POS";
							else if (part.contains("pronoun"))
								data[i] = "PRP";
							else if (part.equalsIgnoreCase("Cardinal number"))
								data[i] = "CD";
							else if (part.equalsIgnoreCase("Cardinal number"))
								data[i] = "CD";
							else if (part.equalsIgnoreCase("Cardinal number"))
								data[i] = "CD";
							else if (part.equalsIgnoreCase("Cardinal number"))
								data[i] = "CD";
							else if (part.equalsIgnoreCase("Cardinal number"))
								data[i] = "CD";
							else if (part.equalsIgnoreCase("Cardinal number"))
								data[i] = "CD";
							else if (part.equalsIgnoreCase("Cardinal number"))
								data[i] = "CD";
						}
						list.add(data);
					}
					line = br.readLine();
				}
				System.out.println("hi");
				br.close();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("/Users/joshualipstone/Documents/Dictionary/" + letter + "1.txt"), "utf-8"));
				/*Pattern part = Pattern.compile("(▶)((\\w|\\.|, )+)");
				for (Pair<String, String> item : list) {
					String def = item.getY();
					StringBuilder types = new StringBuilder(10);
					for (int j = 0; j < def.length(); j++) {
						if (def.charAt(j) == '▶') {
							Matcher m = part.matcher(def);
							m.find(j);
							if (types.length() > 2 && types.charAt(types.length() - 2) == ',')
								types.append(m.group(2));
							else
								types.append(", " + m.group(2));
							j = m.end();
						}
					}
					String temp = types.toString();
					if (temp.contains("suffix") || temp.contains("prefix")) {
						temp = temp.replaceAll(", (suffix|prefix)",  "");
						if (temp.length() == 0)
							continue;
					}
					writer.write(item.getX() + temp + "\n");
				}*/
				writer.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			finally {
				try {
					br.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
