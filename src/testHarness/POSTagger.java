package testHarness;

import java.util.Scanner;

import probabilisticLexer.Descender;
import probabilisticLexer.Lexer;
import probabilisticLexer.Rule;
import probabilisticLexer.Type;
import probabilisticLexer.errors.LexerException;
import probabilisticLexer.helpers.Default;

public class POSTagger {
	private static final Lexer lexer = new Lexer();
	
	public static void main(String[] args) {
		try {
			init(); //Actually need this part to initialize the lexer
			loadExtras(); //Loads the defaults for unknown tokens and an add 1 Smoother, e.g. things that aren't necessary for this to run
			REPL(); //Runs the REPL
		}
		catch (LexerException e) {
			e.printStackTrace();
		}
	}
	
	private static void init() throws LexerException {
		lexer.addRule("Word", new Rule("(([\\w]+)|(``|''|[\\Q.,/?'\"\\\\!@#$%^&*()+=\\E]))", null, null));
		lexer.ignore("[\n\t\r]");
		//These handle parenthesized items in text
		lexer.addDescender("Parentheses", new Descender("(", ")", new Type("parentheses"), null));
		lexer.addDescender("SquareBrackets", new Descender("[", "]", new Type("squareBrackets"), null));
		lexer.addDescender("CurlyBraces", new Descender("{", "}", new Type("curlyBraces"), null));
		lexer.loadCorpus("_", "./tagged/");
	}
	
	private static void REPL() {
		System.out.println("\nPlease enter a sentence to tag, or type HELP: for help.  Type EXIT: to exit.");
		Scanner scanner = new Scanner(System.in);
		String line = "";
		while (!(line = scanner.nextLine()).equals("EXIT:")) {
			try {
				if (line.equals("HELP:"))
					System.out.println("To tag a sentence, just type the sentence.\nType LOAD:,"
							+ " denotes the character(s) that separate the token from its tag to add another file or directory to the active corpus.");
				else if (line.startsWith("LOAD:"))
					lexer.loadCorpus(line.substring(line.indexOf(',') + 1).trim(), line.substring(5, line.indexOf(',')).trim());
				else
					System.out.println(lexer.lex(line));
			}
			catch (LexerException e) {
				e.printStackTrace();
			}
			System.out.println("\nPlease enter another instruction: ");
		}
		scanner.close();
	}
	
	private static void loadExtras() {
		lexer.addDefault("Basic", new Default() {
			
			@Override
			public Type determineType(Object car) {
				if (!(car instanceof String))
					return null;
				String word = (String) car, type = "NNP";
				if (word.endsWith("s"))
					type = "NNS";
				else if (word.charAt(0) >= 65 && word.charAt(0) <= 90)
					type = "NNP";
				else if (word.endsWith("able") || word.endsWith("ible"))
					type = "JJ";
				else if (word.endsWith("ed"))
					type = "VBD";
				return lexer.makeNewDefaultType(type, lexer.getLastSeparator());
			}
		});
		
		lexer.addSmoother("Add 1 Smoothing", Lexer.ADD_1_SMOOTHER);
	}
}
