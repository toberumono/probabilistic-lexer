package testHarness;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;
import java.util.regex.Matcher;

import lipstone.joshua.customStructures.lists.PairedList;
import lipstone.joshua.customStructures.lists.SortedList;
import probabilisticLexer.Action;
import probabilisticLexer.Descender;
import probabilisticLexer.Lexer;
import probabilisticLexer.Rule;
import probabilisticLexer.Token;
import probabilisticLexer.Type;
import probabilisticLexer.errors.LexerException;
import probabilisticLexer.helpers.Default;
import stemmer.Stemmer;

/**
 * Based on code (and in large parts basically a translation of) that Katja and I wrote for the spelling correction project
 * 
 * @author Joshua Lipstone
 */
public class SpellingChecker {
	private static Lexer lexer = new Lexer();
	private static double INSERTION_COST = 1, DELETION_COST = 1, SUBSTITUTION_COST = 2;
	private static int MAX_SUGGESTIONS = 5, MAX_INITIAL_SUGGESTIONS = MAX_SUGGESTIONS * 20;
	private static final Comparator<String> comparator = new Comparator<String>() {
		
		@Override
		public int compare(String o1, String o2) {
			int result = o1.compareTo(o2);
			return result == 0 ? (o1.length() < o2.length() ? -1 : 1) : result;
		}
	};
	private static SortedList<String>[] dictionary = (SortedList<String>[]) Array.newInstance(new SortedList<String>(comparator).getClass(), 26);
	
	public static void main(String[] args) {
		try {
			init(); //Actually need this part to initialize the lexer
			loadExtras(); //Loads the defaults for unknown tokens and an add 1 Smoother, e.g. things that aren't necessary for this to run
			REPL(); //Runs the REPL
		}
		catch (LexerException e) {
			e.printStackTrace();
		}
	}
	
	private static void init() throws LexerException {
		lexer = new Lexer() {
			
			@Override
			public Type makeNewDefaultType(String type, final String typeSeparator) {
				return new Type(type) {
					@Override
					public String valueToString(Object value) {
						if (((String) value).matches("\\w(\\w|\\-|')+"))
							return " " + ((String) value);// + lexer.getLastSeparator() + this.name;
						else
							return ((String) value);// + lexer.getLastSeparator() + this.name;
					}
				};
			}
			
			@Override
			public Action makeNewDefaultAction(final String typeSeparator) {
				return new Action() {
					
					@Override
					public Token action(Matcher match, Lexer lexer, Type... type) throws LexerException {
						String word = match.group();
						if (word.length() > 0) {
							int check = word.charAt(0) - (word.charAt(0) >= 97 ? 97 : 65);
							if (check >= 65 && check <= 91 && dictionary[(check = check - 65)].indexOf(word) == -1)
								dictionary[check].add(word);
							else if (check >= 97 && check <= 123 && dictionary[(check = check - 97)].indexOf(word) == -1)
								dictionary[check].add(word);
						}
						return lexer.makeNewToken(word, null, Type.EMPTY, type);
					}
				};
			}
		};
		lexer.addRule("Word", new Rule("(([\\w]+('[\\w]+|(-[\\w]+)*))|(``|''|[\\Q.,/?'\"\\\\!@#$%^&*()-+=\\E]))", null, null));
		lexer.ignore("[\n\t\r]");
		//These handle parenthesized items in text
		lexer.addDescender("Parentheses", new Descender("(", ")", new Type("parentheses"), null));
		lexer.addDescender("SquareBrackets", new Descender("[", "]", new Type("squareBrackets"), null));
		lexer.addDescender("CurlyBraces", new Descender("{", "}", new Type("curlyBraces"), null));
		for (int i = 0; i < dictionary.length; i++)
			dictionary[i] = new SortedList<String>(500, comparator);
		lexer.loadCorpus("_", "./tagged/");
		try {
			File dictionaries = new File("./dictionary/");
			File[] files = dictionaries.listFiles();
			for (File file : files) {
				if (file.getName().startsWith("."))
					continue;
				Scanner scanner = new Scanner(file);
				if (!scanner.hasNextLine())
					continue;
				String line = scanner.nextLine();
				while (line.length() == 0 && scanner.hasNextLine())
					line = scanner.nextLine();
				if (line.length() == 0)
					continue;
				do {
					while (line.length() == 0 && scanner.hasNextLine())
						line = scanner.nextLine();
					if (line.length() == 0)
						break;
					int check = line.charAt(0);
					dictionary[(check >= 97 ? check - 97 : check - 65)].add(line);
				} while (scanner.hasNextLine() && (line = scanner.nextLine()) != null);
				scanner.close();
			}
		}
		catch (FileNotFoundException e) {}
	}
	
	private static void REPL() throws LexerException {
		System.out.println("\nPlease enter text to correct, or type HELP: for help.  Type EXIT: to exit.");
		Scanner scanner = new Scanner(System.in);
		String line = "";
		while (!(line = scanner.nextLine()).equals("EXIT:")) {
			try {
				if (line.equals("HELP:"))
					System.out.println("To correct a file, type CORRECT:<path to file>.\nType LOAD:<path to tagged file or directory>,"
							+ "<separator> where <separator> denotes the character(s) that separate the token from its tag to add another file or directory to the active corpus.");
				else if (line.startsWith("LOAD:"))
					lexer.loadCorpus(line.substring(line.indexOf(',') + 1).trim(), line.substring(5, line.indexOf(',')).trim());
				else if (line.startsWith("CORRECT:")) {
					try {
						Scanner file = new Scanner(new File(line.substring(8)));
						File output = new File(line.substring(8, line.lastIndexOf('.')) + "_corrected" + line.substring(line.lastIndexOf('.')));
						BufferedWriter writer = new BufferedWriter(new FileWriter(output));
						while (file.hasNextLine())
							writer.write(correctSentence(0, lexer.lex(file.nextLine()), scanner).toString().trim());
						writer.close();
						file.close();
					}
					catch (IOException e) {
						System.out.println(line.substring(8) + ", is not a valid file.");
						continue;
					}
				}
				else
					System.out.println(correctSentence(0, lexer.lex(line), scanner).toString().trim());
			}
			catch (LexerException e) {
				e.printStackTrace();
			}
			System.out.println("\nPlease enter another instruction: ");
		}
		scanner.close();
	}
	
	private static double editDistance(String word1, String word2) {
		word1 = word1.toLowerCase();
		word2 = word2.toLowerCase();
		double distanceTable[][] = new double[word1.length() + 1][word2.length() + 1]; //This implicitly sets all items to 0.0
		for (int i = 1; i < distanceTable.length; i++)
			distanceTable[i][0] = i * INSERTION_COST;
		for (int i = 1; i < distanceTable[0].length; i++)
			distanceTable[0][i] = i * DELETION_COST;
		
		for (int i = 1; i < distanceTable.length; i++) {
			for (int j = 1; j < distanceTable[i].length; j++) {
				distanceTable[i][j] = (word1.charAt(i - 1) == word2.charAt(j - 1) ? 0 : SUBSTITUTION_COST) + distanceTable[i - 1][j - 1];
				if (distanceTable[i - 1][j] + INSERTION_COST < distanceTable[i][j])
					distanceTable[i][j] = distanceTable[i - 1][j] + INSERTION_COST;
				if (distanceTable[i][j - 1] + DELETION_COST < distanceTable[i][j])
					distanceTable[i][j] = distanceTable[i][j - 1] + DELETION_COST;
				if ((word1.charAt(i - 1) == 'i' && word2.charAt(j - 1) == 'y') || (word1.charAt(i - 1) == 'y' && word2.charAt(j - 1) == 'i'))
					distanceTable[i][j] = distanceTable[i][j - 1] + 0.9;
			}
		}
		
		return distanceTable[word1.length()][word2.length()];
	}
	
	private static PairedList<String, Double> betterWords(String word, ArrayList<String>[] dictionary) {
		PairedList<String, Double> suggestions = new PairedList<>();
		//Anchor on first character
		int index = word.charAt(0) - 65;
		if (index >= 32)
			index -= 32;
		String original = word;
		try {
			if (dictionary[index].indexOf(word) > -1 || dictionary[index].indexOf(Stemmer.stem(word)) > -1)
				suggestions.add(word, -1.0);
			//If shortening repeated letter groups to double-letters generates a valid word, then it is probably the correct
			else if (dictionary[index].indexOf(word = word.replaceAll("(\\w)\\1{2,}", "$1$1")) > -1 || dictionary[index].indexOf(Stemmer.stem(word)) > -1)
				suggestions.add(word, -1.0);
			
		}
		catch (lexer.errors.LexerException e) {
			e.printStackTrace();
		}
		word = original;
		
		int charValue = (word.charAt(0) - 65) % 32;
		for (String w : dictionary[index]) {
			if (suggestions.containsKey(w))
				continue;
			if (w.length() > word.length() + 3 || w.length() < word.length() - 2)
				continue;
			if (charValue == (w.charAt(0) - 65) % 32 && w.substring(1).equals(word.substring(1).toLowerCase())) {
				String wo = ((char) (charValue + 65)) + w.substring(1);
				if (suggestions.containsKey(wo))
					continue;
				suggestions.getValues().add(0, -1.0);
				suggestions.getKeys().add(0, wo);
				if (suggestions.size() > MAX_INITIAL_SUGGESTIONS)
					suggestions.remove(suggestions.getKeys().get(suggestions.size() - 1));
				continue;
			}
			int length = w.length() < word.length() ? w.length() : word.length(), count = 0;
			for (int i = 0; i < length; i++)
				if (w.charAt(i) == word.charAt(i))
					count++;
			if (count >= (w.length() < word.length() ? word.length() : w.length()) / 2) {
				double distance = editDistance(w, word);
				int i = 0;
				for (; i < suggestions.size() && i < MAX_INITIAL_SUGGESTIONS; i++)
					if (suggestions.getValues().get(i) > distance) {
						if (suggestions.size() >= MAX_INITIAL_SUGGESTIONS) {
							suggestions.remove(suggestions.getKeys().get(suggestions.size() - 1));
							suggestions.getKeys().add(i, w);
							suggestions.getValues().add(i, distance);
						}
						else {
							suggestions.getKeys().add(i, w);
							suggestions.getValues().add(i, distance);
						}
						break;
					}
				if (i < MAX_INITIAL_SUGGESTIONS && i == suggestions.size()) {
					suggestions.getKeys().add(i, w);
					suggestions.getValues().add(i, distance);
				}
			}
		}
		return suggestions;
	}
	
	private static Token correctSentence(int lineNum, Token sentence, Scanner scanner) throws LexerException {
		Token head = sentence;
		double probability = 1, min = Math.pow(0.01, sentence.length() + 1);
		do {
			if (!head.getCarType().equals("parentheses") && !head.getCarType().equals("squareBrackets") && !head.getCarType().equals("curlyBraces")) {
				if (!((String) head.getCar()).matches("(\\w|\\-|'t)+"))
					continue;
				PairedList<String, Double> suggestions = betterWords((String) head.getCar(), dictionary);
				Token originalToken = head.singular();
				double prob = head.getProbability();
				if (!head.getNextToken().isNull()) {
					if (head.getNextToken().getProbability() == 0)
						prob = originalToken.getProbability();
					else
						prob /= head.getNextToken().getProbability();
				}
				//System.out.println(head.getCarType() + " : " + prob + " : " + head.getTypeProbability());
				if (suggestions.containsKey((String) head.getCar()) && (prob > (min = min / 0.01) || head.getTypeProbability() > 0.01))
					continue;
				PairedList<String, Double> finalSuggestions = new PairedList<>();
				for (String suggestion : suggestions.getKeys()) {
					Token temp = lexer.lex(suggestion);
					head.replaceCar(temp);
					double check = probability * head.getProbability() / suggestions.get(suggestion).get(0);
					//Add to finalSuggestions, if applicable
					int i = 0;
					for (; i < finalSuggestions.size() && i < MAX_SUGGESTIONS; i++)
						if (finalSuggestions.getValues().get(i) > check) {
							if (finalSuggestions.size() >= MAX_SUGGESTIONS) {
								finalSuggestions.remove(finalSuggestions.getKeys().get(i));
								finalSuggestions.getKeys().add(i, suggestion);
								finalSuggestions.getValues().add(i, check);
							}
							else {
								finalSuggestions.getKeys().add(i, suggestion);
								finalSuggestions.getValues().add(i, check);
							}
							break;
						}
					if (i < MAX_SUGGESTIONS && i == finalSuggestions.size()) {
						finalSuggestions.getKeys().add(i, suggestion);
						finalSuggestions.getValues().add(i, check);
					}
				}
				System.out.println("It appears that the word, " + originalToken.getCar() + ", in line " + lineNum + ", is misspelled.  Would you like to change it to:");
				if (finalSuggestions.size() > 0)
					for (int i = 0; i < finalSuggestions.size(); i++)
						System.out.println((i + 1) + ": " + finalSuggestions.getKeys().get(i));
				else
					System.out.println("No suggestions");
				System.out.println("Type the number next to the correct word you would like to use.  To keep the word as is, press enter/return.");
				System.out.println("To change it to any other word, type the word.");
				String response = scanner.nextLine();
				if (response.length() > 0) {
					if (response.matches("\\d+"))
						response = finalSuggestions.getKeys().get(Integer.parseInt(response) - 1);
					head.insert(lexer.lex(response), true);
				}
				else
					head.replaceCar(originalToken);
				probability = head.getProbability() / (!head.getNextToken().isNull() ? head.getNextToken().getProbability() : 1);
			}
			else
				head.replaceCar(lexer.makeNewToken(correctSentence(lineNum, (Token) head.getCar(), scanner), null, Type.EMPTY, head.getCarType()));
		} while (!(head = head.getNextToken()).isNull());
		return sentence;
	}
	
	private static void loadExtras() {
		lexer.addDefault("Basic", new Default() {
			
			@Override
			public Type determineType(Object car) {
				if (!(car instanceof String))
					return null;
				String word = (String) car, type = "NNP";
				if (word.endsWith("s"))
					type = "NNS";
				else if (word.charAt(0) >= 65 && word.charAt(0) <= 90)
					type = "NNP";
				else if (word.endsWith("able") || word.endsWith("ible"))
					type = "JJ";
				else if (word.endsWith("ed"))
					type = "VBD";
				return lexer.hasType(type) ? lexer.getTypes().get(type) : lexer.makeNewDefaultType(type, lexer.getLastSeparator());
			}
		});
		
		//lexer.addSmoother("Add 1 Smoothing", Lexer.ADD_1_SMOOTHER);
	}
}
